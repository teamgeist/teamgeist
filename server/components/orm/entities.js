var ORM     = require('sequelize');
var orm     = require('./orm');
var async   = require('async');
var jwt = require('jsonwebtoken');
var bcrypt  = require('bcrypt');
// var crypto  = require('crypto');
var entities = {};

/* Force update database schema */
var forceSync = false;

// var salt = crypto.randomBytes(16).toString('hex');

// User
entities.User = orm.define('User',
  {
    email: {
        type: ORM.STRING,
        allowNull: false
    },
    password: {
      type: ORM.VIRTUAL,
      set: function (val) {
        this.setDataValue('password', val); // Remember to set the data value, otherwise it won't be validated
        this.setDataValue('passwd_hash', bcrypt.hashSync(val, 10));
      },
    },
    passwd_hash: {
        type: ORM.STRING,
        allowNull: true
    },
    first_name: {
        type: ORM.STRING,
        allowNull: true
    },
    last_name: {
        type: ORM.STRING,
        allowNull: true
    },
    role: {
        type: ORM.ENUM,
        values: [
            'blocked',
            'client',
            'developer',
            'manager',
            'admin'
        ],
        defaultValue: 'developer',
        allowNull: false
    },
    auth_token: {
        type: ORM.STRING,
        allowNull: true
    },
    auth_token_web: {
        type: ORM.STRING,
        allowNull: true
    },
    office_loc_token: {
        type: ORM.STRING,
        allowNull: true
    }
  },
  {
    instanceMethods: {
      generateJwt: function() {
        var expiry = new Date();
        expiry.setDate(expiry.getDate() + 7);
        return jwt.sign({
          _id : this.id,
          email: this.email,
          role: this.role,
          exp: parseInt(expiry.getTime() / 1000),
        }, "MY_SECRET");
      },
      validatePassword: function(password) {
        return bcrypt.compareSync(password, this.passwd_hash);
      }
    },
    tableName: 'user',
    name: {
        singular: 'User',
        plural: 'Users'
    },
    indexes: [
        {
            unique: true,
            fields: [
                'email'
            ]
        },
        {
            unique: true,
            fields: [
                'auth_token'
            ]
        },
        {
            unique: true,
            fields: [
                'auth_token_web'
            ]
        },
        {
            unique: true,
            fields: [
                'office_loc_token'
            ]
        }
    ]
});

// Project
entities.Project = orm.define('Project', {
    name: {
        type: ORM.STRING,
        allowNull: false
    },
    description: {
        type: ORM.STRING,
        allowNull: true
    },
    type: {
        type: ORM.ENUM,
        values: [
            'internal',
            'external'
        ],
        defaultValue: 'internal',
        allowNull: false
    },
    status: {
        type: ORM.ENUM,
        values: [
            'open',
            'close'
        ],
        defaultValue: 'open',
        allowNull: false
    }
}, {
    tableName: 'project',
    name: {
        singular: 'Project',
        plural: 'Projects'
    }
});

// Session
entities.Session = orm.define('Session', {
    description: {
        type: ORM.STRING,
        allowNull: true
    },
    start_time: {
        type: ORM.INTEGER,
        allowNull: false
    },
    stop_time: {
        type: ORM.INTEGER,
        allowNull: false
    },
    status: {
        type: ORM.ENUM,
        values: [
            'started',
            'stopped'
        ],
        defaultValue: 'started',
        allowNull: false
    }
}, {
    tableName: 'tracking_session',
    name: {
        singular: 'Session',
        plural: 'Sessions'
    }
});

// Stat
entities.Stat = orm.define('Stat', {
    creation_time: {
        type: ORM.INTEGER,
        allowNull: false
    },
    creation_day: {
        type: ORM.TEXT,
        allowNull: false
    },
    save_time: {
        type: ORM.INTEGER,
        allowNull: true
    },
    work_time: {
        type: ORM.INTEGER,
        allowNull: true
    },
    image: {
        type: ORM.STRING,
        allowNull: true
    },
    sources: {
        type: ORM.TEXT,
        allowNull: true
    },
    keyboard: {
        type: ORM.INTEGER,
        allowNull: true
    },
    mouse: {
        type: ORM.INTEGER,
        allowNull: true
    },
    token: {
        type: ORM.STRING,
        allowNull: true
    },
    mode: {
        type: ORM.ENUM,
        values: [
            'online',
            'offline'
        ],
        defaultValue: 'online',
        allowNull: false
    },
    status: {
        type: ORM.ENUM,
        values: [
            'pending',
            'completed',
            'rejected'
        ],
        defaultValue: 'pending',
        allowNull: false
    }
}, {
    tableName: 'stat',
    name: {
        singular: 'Stat',
        plural: 'Stats'
    },
    indexes: [
        {
            unique: true,
            fields: [
                'token'
            ]
        }
    ]
});

/* *************************************************** Relations **************************************************** */
// User <===> Project
entities.User.belongsToMany(entities.Project, {
    through: 'user_many_to_many_project',
    foreignKey: 'user_id'
});
entities.Project.belongsToMany(entities.User, {
    through: 'user_many_to_many_project',
    foreignKey: 'project_id'
});

// Project <=== Session
entities.Project.hasMany(entities.Session, {
    foreignKey: 'project_id',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});
entities.Session.belongsTo(entities.Project, {
    foreignKey: 'project_id',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});

// User <=== Session
entities.User.hasMany(entities.Session, {
    foreignKey: 'user_id',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});
entities.Session.belongsTo(entities.User, {
    foreignKey: 'user_id',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});

// Session <=== Stat
entities.Session.hasMany(entities.Stat, {
    foreignKey: 'tracking_session_id',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});
entities.Stat.belongsTo(entities.Session, {
    foreignKey: 'tracking_session_id',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
});

// Schema update
orm.sync({
     force: forceSync
}).then(function () {
    if (! forceSync) {
        return;
    }

    async.waterfall([
        // ...
        function (cb) {
            entities.User.create({
                email: 'ivan@test.test',
                password: 'test',
                first_name: 'First Name',
                last_name: 'Last Name',
                office_loc_token: 'ivan',
                role: 'developer'
            }).then(function (user) {
                cb(null, user);
            });
        }
    ], function (err, user) {
        entities.Project.create({
            name: 'Project Name 1',
            description: 'Description 1',
            status: 'open'
        }).then(function (project) {
            user.addProject(project);
        });
    });

    async.waterfall([
        // ...
        function (cb) {
            entities.User.create({
                email: 'sanya@test.test',
                password: 'test',
                first_name: 'First Name',
                last_name: 'Last Name',
                office_loc_token: 'sanya',
                role: 'developer'
            }).then(function (user) {
                cb(null, user);
            });
        }
    ], function (err, user) {
        entities.Project.create({
            name: 'Project Name 2',
            description: 'Description 1',
            status: 'open'
        }).then(function (project) {
            user.addProject(project);
        });
    });

    async.waterfall([
        // ...
        function (cb) {
            entities.User.create({
                email: 'tolya@test.test',
                password: 'test',
                first_name: 'Tolya',
                last_name: 'Ganzin',
                office_loc_token: 'tolya',
                role: 'developer'
            }).then(function (user) {
                cb(null, user);
            });
        }
    ], function (err, user) {
        entities.Project.create({
            name: 'Project Name 3',
            description: 'Description 1',
            status: 'open'
        }).then(function (project) {
            user.addProject(project);
        });
    });

    async.waterfall([
        // ...
        function (cb) {
            entities.User.create({
                email: 'evgen@test.test',
                password: 'test',
                first_name: 'First Name',
                last_name: 'Last Name',
                office_loc_token: 'evgen',
                role: 'developer'
            }).then(function (user) {
                cb(null, user);
            });
        }
    ], function (err, user) {
        entities.Project.create({
            name: 'Project Name 4',
            description: 'Description 1',
            status: 'open'
        }).then(function (project) {
            user.addProject(project);
        });
    });

    entities.User.create({
        email: 'manager@test.test',
        password: 'test',
        first_name: 'First Name',
        last_name: 'Last Name',
        office_loc_token: 'manager',
        role: 'manager'
    }).then(function (manager) {
          async.waterfall([
          // 1)
            function (cb) {
                entities.User.create({
                    email: 'oleg.krasno@faceit-team.com',
                    password: 'D1K2MYgtS',
                    first_name: 'Олег',
                    last_name: 'Красно',
                    office_loc_token: 'oleg.krasno',
                    role: 'developer'
                }).then(function (user) {
                    cb(null, user);
                });
            }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Красно Олег',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });

          async.waterfall([
              // 2)
              function (cb) {
                  entities.User.create({
                      email: 'alexey.macko@faceit-team.com',
                      password: 'Pnd25GdI4',
                      first_name: 'Алексей',
                      last_name: 'Мацько',
                      office_loc_token: 'alexey.macko',
                      role: 'developer'
                  }).then(function (user) {
                      cb(null, user);
                  });
              }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Мацько Алексей',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });

          async.waterfall([
              // 3)
              function (cb) {
                  entities.User.create({
                      email: 'maksim.shabelnik@faceit-team.com',
                      password: 'WmrWfO3Ky',
                      first_name: 'Максим',
                      last_name: 'Шабельник',
                      office_loc_token: 'maksim.shabelnik',
                      role: 'developer'
                  }).then(function (user) {
                      cb(null, user);
                  });
              }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Шабельник Максим',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });

          async.waterfall([
              // 4)
              function (cb) {
                  entities.User.create({
                      email: 'vladimir.stepanov@faceit-team.com',
                      password: 'Ssn0xidR5',
                      first_name: 'Владимир',
                      last_name: 'Степанов',
                      office_loc_token: 'vladimir.stepanov',
                      role: 'developer'
                  }).then(function (user) {
                      cb(null, user);
                  });
              }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Степанов Владимир',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });

          async.waterfall([
              // 5)
              function (cb) {
                  entities.User.create({
                      email: 'gleb.zaycev@faceit-team.com',
                      password: 'KFYZAN3zJ',
                      first_name: 'Глеб',
                      last_name: 'Зайцев',
                      office_loc_token: 'gleb.zaycev',
                      role: 'developer'
                  }).then(function (user) {
                      cb(null, user);
                  });
              }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Зайцев Глеб',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });

          async.waterfall([
              // 6)
              function (cb) {
                  entities.User.create({
                      email: 'ivan.abakumov@faceit-team.com',
                      password: 'zPFSoYu55',
                      first_name: 'Иван',
                      last_name: 'Абакумов',
                      office_loc_token: 'ivan.abakumov',
                      role: 'developer'
                  }).then(function (user) {
                      cb(null, user);
                  });
              }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Абакумов Иван',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });

          async.waterfall([
              // 7)
              function (cb) {
                  entities.User.create({
                      email: 'alexandr.skripko@faceit-team.com',
                      password: 'fzXBE2Qrq',
                      first_name: 'Александр',
                      last_name: 'Скрыпко',
                      office_loc_token: 'alexandr.skripko',
                      role: 'developer'
                  }).then(function (user) {
                      cb(null, user);
                  });
              }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Скрыпко Александр',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });

          async.waterfall([
              // 8)
              function (cb) {
                  entities.User.create({
                      email: 'igor.polyakov@faceit-team.com',
                      password: 'ptvHQxW3',
                      first_name: 'Игорь',
                      last_name: 'Поляков',
                      office_loc_token: 'igor.polyakov',
                      role: 'developer'
                  }).then(function (user) {
                      cb(null, user);
                  });
              }
          ], function (err, user) {
              entities.Project.create({
                  name: 'Default Project',
                  description: 'Игорь Поляков',
                  status: 'open'
              }).then(function (project) {
                  user.addProject(project);
                  manager.addProject(project);
              });
          });


    });
});

module.exports = entities;

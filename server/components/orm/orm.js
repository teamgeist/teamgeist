var ORM = require('sequelize');
var db = require('./../../config/db');
module.exports = new ORM(db.dbname, db.username, db.password, db.options);

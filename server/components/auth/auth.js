var entities    = require(appRoot.concat('/components/orm/entities'));
var passport    = require('passport');
var User        = entities.User;
var async       = require('async');

module.exports = function (roles) {
    return function (req, res, next) {
        // User.findOne({
        //     where: {
        //         id: 1
        //     }
        // }).then(function (user) {
        //     req.user = user;
        //     next();
        // }).catch(function (err) {
        //     res.status(404).json({
        //         success: false,
        //         data: null,
        //         errors: [
        //             'Пользователь не найден',
        //             JSON.stringify(err)
        //         ]
        //     });
        // });

        var token = req.cookies.auth_token;

        async.waterfall([
            /**
             * @param cb
             */
            function (cb) {
                if (typeof token === 'string') {
                    cb(null);
                } else {
                    res.status(401).json({
                        success: false,
                        data: null,
                        errors: [
                            'Вы не авторизированы'
                        ]
                    });
                }
            },

            /**
             * @param cb
             */
            function (cb) {
                User.findOne({
                    where: {
                        $or: [
                            {
                                auth_token: token
                            },
                            {
                                auth_token_web: token
                            }
                        ]
                    }
                }).then(function (user) {
                    if (user) {
                        cb(null, user);
                    } else {
                        res.status(401).json({
                            success: false,
                            data: null,
                            errors: [
                                'Вы не авторизированы'
                            ]
                        });
                    }
                }).catch(function (err) {
                    res.status(400).json({
                        success: false,
                        data: null,
                        errors: [
                            'Ошибка при проверке авторизации',
                            JSON.stringify(err)
                        ]
                    });
                });
            },

            /**
             * @param user
             * @param cb
             */
            function (user, cb) {
                if (roles.indexOf(user.role) > -1) {
                    cb(null, user);
                } else {
                    res.status(403).json({
                        success: false,
                        data: null,
                        errors: [
                            'Недостаточно прав'
                        ]
                    });
                }
            }
        ], function (err, user) {
            req.user = user;
            next();
        });
    };
};

// var time = require('locutus/php/datetime/time');
// var mt_rand = require('locutus/php/math/mt_rand');

var locutus  = require('locutus');
var dbConfig = require('./../config/db');
var intervalSize = 600;

var helper = {
    /**
     * Get current UNIX TIMESTAMP
     */
    time: locutus.php.datetime.time,

    /**
     * Mersenne twister
     */
    mt_rand: locutus.php.math.mt_rand,

    /**
     * Base64 encode
     */
    base64_encode: locutus.php.url.base64_encode,

    /**
     * Base64 decode
     */
    base64_decode: locutus.php.url.base64_decode,

    /**
     * Get interval by time
     * @param ts
     * @returns {Number}
     */
    getIntervalByTime: function (ts) {
        return parseInt(ts / intervalSize);
    },

    /**
     * Get next screen time (unix timestamp)
     * @returns {Number}
     */
    nextScreenTime: function () {
        return this.mt_rand(
            this.nextInterval() * intervalSize + 10,
            this.nextInterval() * intervalSize + intervalSize - 10
        );
    },

    /**
     * Get offset screen time (seconds)
     * @returns {number}
     */
    nextScreenTimeOffset: function () {
        return this.nextScreenTime() - this.time();
    },

    /**
     * Get current interval
     * @returns {Number}
     */
    currentInterval: function () {
        return parseInt(this.time() / intervalSize);
    },

    /**
     * Get next interval
     * @returns {Number}
     */
    nextInterval: function () {
        return this.currentInterval() + 1;
    },

    /**
     * Get random string
     * @param len
     * @returns {string}
     */
    randomString: function (len) {
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < len; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },

    /**
     * SQL string edit
     * @param {string} sql
     * @returns {string}
     */
    sqlConvert: function (sql) {
        const list = {
            mysql: '`',
            postgres: '"'
        };

        return sql.replace(/\`{1}|\"{1}/g, list[dbConfig.options.dialect]);
    }
};

module.exports = helper;

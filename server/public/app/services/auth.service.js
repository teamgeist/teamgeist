/* Authenticate service */

angular.module('app').factory('Auth', function ($location, $rootScope, $http, $window, $q, $state) {

  var login = function (user) {
    return $http.post('/dashboard-api/v1/auth/sign-in', {
      email: user.email,
      passwd: user.password
    })
    .success(function (response) {
      console.log('logging in..');
      saveToken(response.token);
    });
  };

  var logout = function () {
    console.log('logging out..');
    currentUser = {};
    projects = [];
    $window.localStorage.removeItem('token');
    $state.go('login');
  };

  var saveToken = function (token) {
      $window.localStorage.token = token;
  };

  var getToken = function () {
    return $window.localStorage.token;
  };

  var isLoggedIn = function () {
    var token = getToken();
    var payload;

    if (token) {
      payload = token.split('.')[1];
      payload = $window.atob(payload);
      payload = JSON.parse(payload);

      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  };

  var currentUser = function () {
    if(isLoggedIn()){
      var token = getToken();
      var payload = token.split('.')[1];
      payload = $window.atob(payload);
      payload = JSON.parse(payload);
      return {
        id    : payload._id,
        email : payload.email,
        role  : payload.role
        // name : payload.name
      };
    }
  };




  return {
    /**
     * Authenticate user
     *
     * @param  {Object}   user     - login info
     * @param  {Function} callback - optional
     * @return {Promise}
     */
    login: login,

    /**
     * Delete user info
     *
     * @param  {Function}
     */
    logout: logout,

    saveToken: saveToken,

    getToken: getToken,

    isLoggedIn: isLoggedIn,
    /**
     * Gets all available info on authenticated user
     *
     * @return {Object} user
     */
    currentUser: currentUser,

    /**
      * Set new url from ConfigController
      *
      * @param {string} new url to use
      * @return {string} url
      */
    setUrl: function(newUrl) {
      url = newUrl;
    },

    /**
      * Get url
      *
      * @return {string} url
      */
    getUrl: function() {
      return url;
    },

    /**
      * Get offline token for our Api
      * Offline token is required for offline activity route
      *
      * @return {string} offline token
      */
    getOfflineToken: function() {
      return offlineToken;
    }
  };
});

angular.module('app').factory('SessionService', function ($state, $location, Auth) {
//подключил Auth service


  var getStageAccess = function (event, toState) {

      var userRole = Auth.currentUser();

        if (toState.data !== undefined) {
          if (toState.data.noLogin !== undefined && toState.data.noLogin) {

            if (Auth.isLoggedIn() && (userRole.role === toState.parent || userRole.role !== toState.name) && userRole.role!=='undefined') {
              // console.log('no ok true');
               event.preventDefault();
               $state.go(userRole.role+'.detail');
            }
            // если нужно, выполняйте здесь какие-то действия
            // перед входом без авторизации
          }
        }

        else {
          // вход с авторизацией
            if (Auth.isLoggedIn() && (userRole.role === toState.parent || userRole.role === toState.name)) {
              //если пользователь авторищован и у него есть права перехода на стрницу
            //  console.log('ok');
            } else {
              // если пользователь авторизован, но у него нет прав для перехода - отпраляеться на страницу входа
            //  console.log('no');
              event.preventDefault();
              //if(toStatename)
              $state.go('login');
            }
          }

        }

  return {
    getStageAccess: getStageAccess
  }
});

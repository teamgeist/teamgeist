angular.module('app').factory('Statistic', function ($resource, $stateParams) {

  return $resource('/dashboard-api/v1/statistic/:controller',
  {
    controller : '@controller',
    id : '@id'
  },
  {

    // {
    //     start_time: 1400000000,
    //     end_time: 1500000000,
    //     project_id: 3,
    //      user_Id: 3
    // }
    getProjectStatistic: {
      method: 'POST',
      params: {
        controller: 'statistics-for-period',
      },
    },

    deleteStatistic: {
      method: 'POST',
      params: {
        controller: 'delete',
        id: '@id',
      },
    },

  });
});

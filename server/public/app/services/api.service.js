
/* Api service */
angular.module('app').factory('Api', function ($resource, $rootScope, $stateParams) {

  return $resource('/:api/v1/:route/:controller/:ymd/:token',
  {
    api: '@api',
    route : '@route',
    controller : '@controller',
    ymd: '@ymd',
    token: '@token'
  },
  {
    getUserDayStatistic: {
      method: 'GET',
      params: {
        api: 'api',
        route: 'office-loc',
        controller: 'user-stat',
      }
    }
  });
});

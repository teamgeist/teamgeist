angular.module('app').factory('DialogService', function ($mdDialog) {

    function DialogImageController($scope, $mdDialog, stat) {
      $scope.stat = stat;
      $scope.cancel = function() {
        $mdDialog.cancel();
      };
    }
    function DialogTimeController($scope, $mdDialog, title) {
      $scope.title = title;
      $scope.hide = function() {
       $mdDialog.hide();
      };
      $scope.cancel = function() {
       $mdDialog.cancel();
      };
      $scope.answer = function(answer) {
       $mdDialog.hide(answer);
      };
    }


    var showScrean = function(ev, stat, cb) {
      console.log(stat);
      $mdDialog.show({
        controller: DialogImageController,
        templateUrl: 'app/views/dialog.img.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: true,
        locals: {
          stat:stat
        }
      })
      .then(function(answer) {
        cb(answer);
      });
    };

    var showDayTime = function(ev, cb) {
      $mdDialog.show({
        controller: DialogTimeController,
        templateUrl: 'app/views/dialog.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: true,
        locals: {
          title: 'Day total time'
        }
      })
      .then(function(answer) {
        cb(answer);
      });
    };

    var showProjectTime = function(ev, cb) {
      $mdDialog.show({
        controller: DialogTimeController,
        templateUrl: 'app/views/dialog.tmpl.html',
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true,
        fullscreen: true,
        locals: {
          title: 'Project total time'
        }
      })
      .then(function(answer) {
        cb(answer);
      });
    };


  return {
    showScrean: showScrean,
    showDayTime: showDayTime,
    showProjectTime: showProjectTime
  };
});

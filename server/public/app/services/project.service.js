angular.module('app').factory('Project', function ($resource, $stateParams) {

  return $resource('/dashboard-api/v1/project/:controller/:id',
  {
    controller : '@controller',
    id : '@id'
  },
  {
    createProject: {
      method: 'POST',
      params: {
        controller: 'create',
      },
    },

    getProjectInfo: {
      method: 'GET',
      params: {
        controller: 'view',
      },
      // transformResponse: function (response) {
      //     $rootScope.selectedProject = angular.fromJson(response).data.project;
      //     $rootScope.attachedDevelopers = angular.fromJson(response).data.project.Users;
      // }
    },

    getProjectsList: {
      method: 'GET',
      params: {
        controller: 'list',
      },
    }

  });
});

angular.module('app').factory('User', function ($resource, $stateParams) {

  return $resource('/dashboard-api/v1/user/:controller/:id',
  {
    controller : '@controller',
  },
  {
    getCurrentUserProjects: {
      method: 'GET',
      params: {
        controller: 'projects-current-user'
      }
    },
    getUserProjects: {
      method: 'GET',
      params: {
        controller: 'projects'
      }
    },

    getUsersList: {
      method: 'GET',
      params: {
        controller: 'list'
      }
    },
    getUserById: {
      method: 'GET',
      params: {
        controller: 'one-user'
      }
    },
    getCurrentUser: {
      method: 'GET',
      params: {
        controller: 'current-user'
      }
    },

    setUser: {
      method: 'POST',
      params: {
        controller: 'add-user'
      }
    },

    removeUser: {
      method: 'POST',
      params: {
        controller: 'remove-user'
      }
    },
    createUser: {
      method: 'POST',
      params: {
        controller: 'create'
      }
    }
  });
});

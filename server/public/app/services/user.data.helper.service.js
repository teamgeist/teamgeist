angular.module('app').factory('UserDataService', function (User, Statistic, $filter) {

    function parseTime(seconds) {
      if (!seconds) {
        return '0h 0m';
      }
      return Math.floor(moment.duration(seconds, 'seconds').asHours()) + 'h ' + moment.duration(seconds, 'seconds').minutes() +'m';
    };
    function parseMinutes(seconds) {
      if (!seconds) {
        return '0m';
      }
      return moment.duration(seconds, 'seconds').minutes() +'m';
    };

    function getUserData (id, cb) {
      User.getUserById({ id: id })
      .$promise.then(function(res) {
        cb(res.data.user);
      });
    }
    function getUserProjects (id, cb) {
      User.getUserProjects({ id: id })
      .$promise.then(function(res) {
        cb(res.data.projects);
      });
    }

    function getStartEndTime(date, view)
    {
      return {start: moment(date).startOf(view).unix(), end: moment(date).endOf(view).unix()};
    }
    function getMonthStat(data, projectId, userId, cb) {
      var mon = moment(data).startOf('month').toDate();
      var week = mon;
      var time = {start:  moment(data).startOf('month').unix(), end: moment(data).endOf('month').unix()};
      var stat = {monthName: moment(mon).format('MMMM'), monthStat:'',weeks:[]};

      Statistic.getProjectStatistic({ start_time: time.start, end_time: time.end, project_id: projectId, user_id: userId})
        .$promise.then(function(resM) {

          stat.monthStat = totalTime(resM.data.stats, time);

          async.whilst(
             function() { return moment(week).startOf('week').unix() < moment(data).endOf('month').unix(); },
             function(callback) {

               time = {start:  moment(week).startOf('week').unix(), end: moment(week).endOf('week').unix()};
               Statistic.getProjectStatistic({ start_time: time.start, end_time: time.end, project_id: projectId, user_id: userId})
               .$promise.then(function(resW) {
                 var days = [];
                 var weekStat = totalTime(resW.data.stats, time);

                 var day = moment(week).startOf('week').toDate();
                 var i=0;

                 async.whilst(
                   function() { return i<7; },
                   function(callbackDay) {

                      time = {start:  moment(day).startOf('day').unix(), end: moment(day).endOf('day').unix()};

                      Statistic.getProjectStatistic({ start_time: time.start, end_time: time.end, project_id: projectId, user_id: userId})
                      .$promise.then(function(resD) {

                        days.push({dayStat:totalTime(resD.data.stats, time), date:moment(day).toDate()});
                        day = moment(day).add(1,'d');
                        i++;
                        callbackDay(null, day);

                      });
                   },
                   function (err, n) {}
                 );

                 stat.weeks.push({weekStat: weekStat, days:days});
                 week = moment(week).add(7,'d');
                 callback(null, week);

               });
             },
             function (err, n) {}
          );

        });

        cb(stat);

    }

    function totalTime(res,time) {
      var totalTime = 0;
      for (var i = 0; i < res.length; i++) {
        totalTime += res[i].work_time;
      }
      return parseTime(totalTime);
    }

    function getUnique(arr) {
        var i = 0,
        current,
        length = arr.length,
        unique = [];
        for (; i < length; i++) {
          current = arr[i];
          if (!~unique.indexOf(current)) {
            unique.push(current);
          }
        }
        return unique;
      };
    function getStat(date, view, projectId, userId, cb) {

      var time = getStartEndTime(date, view);

      Statistic.getProjectStatistic({ start_time: time.start, end_time: time.end, project_id: projectId, user_id: userId})
      .$promise.then(function(res) {
        if(res.data.stats.length) {
          var array = [];

          for(var i=0; i < res.data.stats.length; i++) {
            if(!res.data.stats[i].mouse && !res.data.stats[i].keyboard && res.data.stats[i].work_time<60) {
              res.data.stats[i].image = "offline.png";
              res.data.stats[i].id = "";
            }
            if(res.data.stats[i].mouse || res.data.stats[i].keyboard || res.data.stats[i].work_time>59) {
              array.push(res.data.stats[i])
            }
          }


          var hours = [];
          var hourTime = new Date(array[0].created_at);
          hours.push(hourTime.getHours());
          var j = 0;
          for (var i = 1; i < array.length; i++) {
            hourTime = new Date(array[i].created_at);
            if(hourTime.getHours() != hours[j]) {
              hours.push(hourTime.getHours());
              j++;
            }
          }
          hours=getUnique(hours);
          hours.sort(function(a,b){
            return a - b;
          });

          var stat = [];
          for (j = 0; j < hours.length; j++) {
            var hour = [];
            for (var i = 0; i < array.length; i++) {
              hourTime = new Date(array[i].created_at);
              if(hourTime.getHours() == hours[j]) {
                hour.push(array[i]);
              }
            }
            hour.sort(function(a,b){
              return new Date(a.created_at) - new Date(b.created_at);
            });

            if(hour.length > 5) {
              var tmp = hour;
              hour=[];
              for (var i = 0; i < tmp.length; i++) {
                if (i < tmp.length-1) {
                  var a = new Date(tmp[i].created_at);
                  var b = new Date(tmp[i+1].created_at);

                  if(parseInt(a.getMinutes()/10) == parseInt(b.getMinutes()/10)){
                    if(tmp[i].work_time>tmp[i+1].work_time) {
                      hour.push(tmp[i]);
                    }
                    else {
                      hour.push(tmp[i+1]);
                    }
                    i++;
                  }
                  else {
                    hour.push(tmp[i]);
                  }
                }
                else {
                  hour.push(tmp[i]);
                }
              }
            }
            if(hour.length < 6) {
              var tmpHour = hour;
              hour=[];
              var k=0;
              for (var i = 0; i < 6; i++) {
                if(k < tmpHour.length){
                  hourTime = new Date(tmpHour[k].created_at);
                }
                if(parseInt(hourTime.getMinutes()/10) == i)
                {
                  hour.push(tmpHour[k]);
                  k++;
                }
                else {
                  var time = new Date(tmpHour[0].created_at);
                  time.setMinutes(i*10);
                  hour.push({image:"offline.png", created_at:time, mouse:0, keyboard:0, work_time:0});
                }
              }
            }
            var hourTotalTime = 0;
            for (var i = 0; i < 6; i++) {
              hourTotalTime += hour[i].work_time;
              hour[i].work_time = parseMinutes(hour[i].work_time);
            }

            stat.push({hourTotal:parseTime(hourTotalTime), hour});
          }

          cb(stat);
        }
        else {
          cb([]);
        }
      });
    }

    function getDayTotalTime(date, view, projectId, userId, cb) {

      var time = getStartEndTime(date, view);

      Statistic.getProjectStatistic({ start_time: time.start, end_time: time.end, project_id: projectId, user_id: userId})
      .$promise.then(function(res) {
        var totalTime = 0;
        for (var i = 0; i < res.data.stats.length; i++) {
          totalTime += res.data.stats[i].work_time;
        }
        cb (parseTime(totalTime));
      });
    }

    function getProjectTotalTime(date, view, projectId, userId, cb) {

      date = new Date();
      var time = getStartEndTime(date, view);
      time.start = 0;

      Statistic.getProjectStatistic({ start_time: time.start, end_time: time.end, project_id: projectId, user_id: userId})
      .$promise.then(function(res) {
        var totalTime = 0;
        for (var i = 0; i < res.data.stats.length; i++) {
          totalTime += res.data.stats[i].work_time;
        }
        cb (parseTime(totalTime));
      });
    }


  return {
    getStat:              getStat,
    parseTime:            parseTime,
    getUserData:          getUserData,
    getMonthStat:         getMonthStat,
    getDayTotalTime:      getDayTotalTime,
    getUserProjects:      getUserProjects,
    getStartEndTime:      getStartEndTime,
    getProjectTotalTime:  getProjectTotalTime
  };
});

(function() {
  'use strict';

  function defaultInterceptor($q, $window) {
    return {
      request: function(config) {
        config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
        return config;
      },

      requestError: function(config) {
        return config;
      },

      response: function(res) {
        return res;
      },

      responseError: function(res) {
        return $q.reject(res);
      }
    };
  }

  config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider'];

  function config($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/login');
    $httpProvider.interceptors.push('defaultInterceptor');

      $stateProvider
        .state('login', {
          url:          '/login',
          templateUrl:  'app/views/login.html',
          controller:   'MainController',
          controllerAs: 'main',
          data: {
            'noLogin': true
          }
        })
        //абстрактный state для выноса основной разметки и функционала
        .state('dashboard',{
          url:          '/dashboard',
          abstract:     true,
          templateUrl:  'app/views/dashboard.html',
          controller:   'dashboardController',
          controllerAs: 'dashboard'
        })
        //абстрактный state для промежуточной обработки и вспомогательной разметки (частично дозаполняет представление)
        //при необходимости можно добавить контроллеры промежуточной обработки
        .state('developer', {
          url:      '/developer',
          abstract: true,
          parent:   'dashboard',
          views: {
             'title@dashboard': {
               template:  '<h5>Developer</h5>'
             },
             'sideNavContent@dashboard': {
               templateUrl: 'app/views/developer/side-nav-content.html'
             }
           }
        })
        //state с разметкой и контроллером для контента главной карточки
        .state('developer.detail', {
          url:    '/projects',
          parent: 'developer',
          views: {
            'cardContent@dashboard': {
              templateUrl:  'app/views/developer/partials/developer-view.html',
              controller:   'DeveloperController',
              controllerAs: 'developer'
            }
          }
        })
        //download client
        .state('developer.download', {
          url:    '/download/client',
          parent:   'developer',
          views: {
            'cardContent@dashboard': {
              templateUrl:  'app/views/download/client-view.html'
            }
          }
        })
        .state('manager.download', {
          url:    '/download/client',
          parent:   'manager',
          views: {
            'cardContent@dashboard': {
              templateUrl:  'app/views/download/client-view.html'
            }
          }
        })
        // аналогичен state developer, но с другим представлением
        .state('manager', {
          abstract: true,
          parent:   'dashboard',
          url:      '/manager',
          views: {
            'title@dashboard': {
              templateUrl: 'app/views/manager/title.html'
            },
            'sideNavContent@dashboard': {
              templateUrl:  'app/views/manager/side-nav-content.html',
              controller:   'ManagerController',
              controllerAs: 'manager'
            }
          }
        })
        .state('manager.detail', {
          parent: 'manager',
          url:    '/detail/:projectId',
          views: {
            'cardContent@dashboard': {
              templateUrl: 'app/views/manager/partials/manager-detail-view.html',
              controller: 'ManagerDetailController',
              controllerAs: 'managerDetail'
            }
          }
        })
        .state('manager.projects', {
          parent: 'manager',
          url:    '/projects',
          views: {
            'cardContent@dashboard': {
              templateUrl:  'app/views/manager/partials/projects-view.html',
              controller:   'ManagerProjectsController',
              controllerAs: 'managerProjects'
            }
          }
        })
        .state('manager.developers', {
          parent: 'manager',
          url:    '/developers',
          views: {
            'cardContent@dashboard': {
              templateUrl:  'app/views/manager/partials/developers-view.html',
              controller:   'ManagerDevelopersController',
              controllerAs: 'managerDevelopers'
            }
          },
          resolve: {
            User: 'User',
            getUsersList: function(User) {
              return User.getUsersList().$promise.then(function(res) {
                return res.data.users;
              });
            }
          }
        })
        .state('manager.developer', {
          parent: 'manager',
          url:    '/developer/:userId/:projectId',
          views: {
            'cardContent@dashboard': {
              templateUrl:  'app/views/manager/partials/developer-view.html',
              controller:   'ManagerDeveloperDetailController',
              controllerAs: 'managerDeveloperDetail'
            }
          }
        })
        .state('manager.project-view', {
          parent: 'manager',
          url:    '/project-view/:projectId',
          views: {
            'cardContent@dashboard': {
              templateUrl:  'app/views/manager/partials/project-view.html',
              controller:   'ManagerProjectController',
              controllerAs: 'managerProjectView'
            }
          },
          resolve: {
            Project:  'Project',
            User:     'User',
            // getProjectInfo2: function(Project, $stateParams) {
            //   return Project.getProjectInfo({ id: $stateParams.id }).$promise.then(function(res) {
            //     return res.data.project;
            //   });
            // },
            getUsersList: function(User) {
              return User.getUsersList().$promise.then(function(res) {
                return res.data.users;
              });
            }
          }
        });
        // .state('client', {
        //   url: '/client',
        //   templateUrl: 'app/views/client/dashboard.html',
        //   controller: 'ClientController',
        //   controllerAs: 'client'
        // });

  }

  runApp.$inject = ['$rootScope', '$state', '$stateParams', '$location', '$resource', '$timeout', 'Auth', 'SessionService', 'authManager'];

  function runApp($rootScope, $state, $stateParams, $location, $resource, $timeout, Auth, SessionService, authManager) {
    authManager.checkAuthOnRefresh();
    authManager.redirectWhenUnauthenticated();
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.currentUser = {};


///////////////////////////////////////////////
    $rootScope.$on('$stateChangeStart',
      function (event, toState) {
         SessionService.getStageAccess(event, toState);
      }
    );
///////////////////////////////////////////////


    $rootScope.getWorkTime = getWorkTime;

    function getWorkTime(seconds) {
      if (!seconds) {
        return '0h 0m';
      }
      return Math.floor(moment.duration(seconds, 'seconds').asHours()) + 'h ' + moment.duration(seconds, 'seconds').minutes() +'m';
    }

    $rootScope.$on('$viewContentLoaded', function () {
          $timeout(function () {
            componentHandler.upgradeAllRegistered();
          });
        });
  }

  angular
    .module('app', [
            'ui.router',
            'ngResource',
            'ngMaterial',
            'ngCookies',
            'ngMessages',
            'ngAnimate',
            'angular-jwt',
      ])
    .factory('defaultInterceptor', defaultInterceptor)
    // .factory('config', config)
    .config(config)
    .run(runApp);

})();

(function(){
  'use strict';

  angular
    .module('app')
    .controller('ManagerDevelopersController', ManagerDevelopersController);

    ManagerDevelopersController.$inject = ['$scope', '$stateParams', '$rootScope', '$state', 'Auth', 'Api', '$mdMedia', 'getUsersList', 'User'];

    function ManagerDevelopersController($scope, $stateParams, $rootScope, $state, Auth, Api, $mdMedia ,getUsersList, User) {
      var vm = this;
      function initUsersList() {
        User.getUsersList()
        .$promise.then(function(res) {
          vm.users = res.data.users;
          vm.newDeveloper = null;
        });
      }

      vm.users = null;
      initUsersList();

      vm.addDeveloper = function(dev) {
      
        User.createUser(dev)
        .$promise.then(function(res) {
          initUsersList();
        })
      };

    }
})();

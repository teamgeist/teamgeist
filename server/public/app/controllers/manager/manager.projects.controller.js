(function(){
  'use strict';

  angular
    .module('app')
    .controller('ManagerProjectsController', ManagerProjectsController);

    ManagerProjectsController.$inject = ['$scope', '$stateParams', '$state', 'Auth', 'Project', '$resource', '$mdMedia'];

    function ManagerProjectsController($scope, $stateParams, $state, Auth, Project, $resource, $mdMedia) {
      var vm = this;
      function initProjects() {
        Project.getProjectsList().$promise.then(function(res) {
          vm.projectsList = res.data.projects;
        });
      }

      vm.projectsList = [];
      vm.newProject = null;
      initProjects();

      vm.createProject = function (data) {
        Project.createProject(data);
        initProjects();
        vm.newProject = null;
      }


    }
})();

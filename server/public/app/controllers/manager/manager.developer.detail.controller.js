(function(){
  'use strict';

  angular
    .module('app')
    .controller('ManagerDeveloperDetailController', ManagerDeveloperDetailController);

    ManagerDeveloperDetailController.$inject = ['$stateParams', 'Auth', 'User', 'Statistic', 'DialogService', 'UserDataService'];

    function ManagerDeveloperDetailController($stateParams, Auth, User, Statistic, DialogService, UserDataService) {
      var vm = this;
      //init User
      vm.selectedProjectId = $stateParams.projectId;
      vm.userId = $stateParams.userId;

      vm.userStat = [];
      vm.dayTotal = 0;
      vm.customFullscreen = false;
      vm.selectedDate = new Date();
      vm.selectedIndex = 0;
      vm.getProjectStatistic = getProjectStatistic;


      UserDataService.getUserData(vm.userId, function(res) {
        vm.user = res;
      });

      UserDataService.getUserProjects(vm.userId, function(res) {
        vm.projects = res;
        if(res.length) {
          vm.selectedProjectId = res[res.length-1].id;
        }
        getProjectStatistic();
      });

      //Date
      vm.selectedDay = function(date) {
        vm.selectedDate = date;
        vm.selectedIndex = 0;
        vm.getProjectStatistic();
      };
      vm.getTodayDate = function() {
        var today = new Date();
        today.setHours(0,0,0,0);
        vm.selectedDay(today);
      };

      //Statistic
      function getProjectStatistic() {
        UserDataService.getDayTotalTime(vm.selectedDate, 'day', vm.selectedProjectId, vm.userId, function(res) {
          vm.dayTotal = res;
        });
        UserDataService.getProjectTotalTime(vm.selectedDate, 'day', vm.selectedProjectId, vm.userId, function(res) {
          vm.projectTotal = res;
        });

        if(vm.selectedIndex) {
          UserDataService.getMonthStat(vm.selectedDate, vm.selectedProjectId, vm.userId, function(res){
            vm.userMonthStat = res;
          });
        }
        else {
          UserDataService.getStat(vm.selectedDate, 'day', vm.selectedProjectId, vm.userId, function(res) {
            vm.userStat = res;
          });
        }
      };

      vm.deleteStatistic = function(id)
      {
        Statistic.deleteStatistic({id:id})
        .$promise.then(function (res) {
          if (res.data!==null) {
            vm.getProjectStatistic();
          }
          else {
            console.log(res.errors);
          }
        });
      }

      //Dialogs
      vm.showScrean = function(ev, stat) {
        DialogService.showScrean(ev, stat, function (answer) {/*do something with answer*/});
      };
      vm.showDayTime = function(ev) {
        DialogService.showDayTime(ev, function (answer) {/*do something with answer*/});
      };
      vm.showProjectTime = function(ev) {
        DialogService.showProjectTime(ev, function (answer) {/*do something with answer*/});
      };

    }
})();

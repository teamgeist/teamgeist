(function(){
  'use strict';

  angular
    .module('app')
    .controller('ManagerProjectController', ManagerProjectController);

    ManagerProjectController.$inject = ['$scope', '$stateParams', '$rootScope', '$state', 'Auth', 'Api', '$mdMedia', 'getUsersList', 'User', 'Project', 'Statistic', 'UserDataService'];

    function ManagerProjectController($scope, $stateParams, $rootScope, $state, Auth, Api, $mdMedia, getUsersList, User, Project, Statistic, UserDataService) {
      var vm = this;
       //vm.attachedDevelopers = getProjectInfo2.Users;
      function updateProjectInfo() {
         Project.getProjectInfo({ id: $stateParams.projectId })
          .$promise.then(function(res) {
            vm.getProjectInfo = res.data.project;
            var data = [];
            var users = res.data.project.Users;

            var i=0;
            async.whilst(
              function () { return i<users.length; },
              function (callback) {
                UserDataService.getProjectTotalTime(Date(), 'day', $stateParams.projectId, users[i].id, function(res){
                  data.push({user: users[i], workedTime: res});
                  i++;
                  callback(null, data)
                });
              },
              function (err, n) {}
            );
            vm.attachedDevelopers = data;
          });
      };

      vm.users = getUsersList;
      vm.startDayTime = getTodayDateStart();
      vm.nowDayTime = getTodayDateNow();
      updateProjectInfo();

      vm.getWorkedTime = function(id) {
        return 0;
      }

      vm.detail = function (user) {
        if(user.role === "manager") {
          $state.go('manager.detail', { projectId: $stateParams.projectId });
        }
        else {
          $state.go('manager.developer', { userId: user.id, projectId: $stateParams.projectId });
        }
      }

      vm.isHideDev = function (id) {
        var res=false;
        angular.forEach(vm.attachedDevelopers,function (dev) {
          if(dev.id==id) {
            res = true;
          }
        });
        return res;
      };


      vm.setUser = function setUser(userId) {
        User.setUser({
          user_id: userId,
          project_id: $stateParams.projectId
        });
        updateProjectInfo();
      };
      vm.removeUser = function removeUser(userId) {
        User.removeUser({
          user_id: userId,
          project_id: $stateParams.projectId
        });
        updateProjectInfo();
      };



      function getTodayDateStart() {
        vm.selectedDate = new Date();
        vm.selectedDate.setHours(0,0,0,0);
        return vm.selectedDate.getTime();
      }
      function getTodayDateNow() {
        vm.selectedDate = new Date();
        return vm.selectedDate.getTime();
      }

    }
})();

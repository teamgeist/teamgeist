(function(){
  'use strict'

  angular
    .module('app')
    .controller('dashboardController', dashboardController);

    dashboardController.$ingect = ['$mdSidenav', '$mdMedia', '$scope', '$mdBottomSheet', 'Auth'];

    function dashboardController($mdSidenav, $mdMedia, $scope, $mdBottomSheet, Auth) {
      var vm = this;
      vm.addClass = !$mdMedia('gt-sm');

      vm.logout = function() {
        console.log('logout');
        $mdSidenav('left').toggle();
        Auth.logout();
      }

      $scope.$watch(function() { return $mdMedia('gt-sm'); }, function(sm) {
       vm.addClass = sm;
     });

      vm.menu = function() {
        $mdSidenav('left').toggle();
      };

    }
  })();

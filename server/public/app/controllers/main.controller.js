(function(){
  'use strict';

  angular
    .module('app')
    .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$rootScope', '$state', 'Auth', 'Api'];

    function MainController($scope, $rootScope, $state, Auth, Api) {
      var vm = this;
      vm.error_message = '';
      vm.loginUser = loginUser;

      function loginUser() {
        Auth.login(vm.currentUser)
        .error(function (err) {
          vm.error_message = err;
          return false;
        })
        .then(function (response) {
          var user = Auth.currentUser();          
          switch (user.role) {
            case 'developer':
              $state.go('developer.detail');
              break;
            case 'manager':
              $state.go('manager.detail');
              break;
            case 'client':
              $state.go('client');
              break;
          }
        });
      }
    }
})();

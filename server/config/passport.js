var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var entities = require('../components/orm/entities');
var User = entities.User;

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'passwd'
  },
  function(email, passwd, done) {
    User.findOne({
      where: {
        email: email,
        // role: [
        //   'developer'
        // ]
      }
    }).then(function (user) {
        // Return if user not found in database
        if (!user) {
          return done(null, false, {
            message: 'User not found'
          });
        }
        // Return if password is wrong
        if (!user.validatePassword(passwd)) {
          return done(null, false, {
            message: 'Password is wrong'
          });
        }

        // If credentials are correct, return the user object
        return done(null, user);
      }).catch(function (err) {
          if (err) {
            return done(err);
          }
      });

  }
));

module.exports = {
    username: 'root',
    password: 'root',
    dbname: 'teamgeist',
    options: {
        host: '127.0.0.1',
        port: 3306,
        dialect: 'mysql',
        logging: console.log,
        maxConcurrentQueries: 100,
        define: {
            charset: 'utf8',
            collate: 'utf8_unicode_ci',
            timestamps: true,
            underscored: true
        },
        pool: {
            maxConnections: 20,
            minConnections: 0,
            maxIdleTime: 30000
        }
    }
};

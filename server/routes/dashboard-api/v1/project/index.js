var express     = require('express');
var router      = express.Router();
// var async       = require('async');
var entities    = require(appRoot.concat('/components/orm/entities'));
var User        = entities.User;
var Project     = entities.Project;
var Session     = entities.Session;
var Stat        = entities.Stat;
var auth        = require(appRoot.concat('/components/auth/auth'));

/* ************************************************* Project list *************************************************** */
router.get('/list', function(req, res) {
  // TODO:
  // User.findById(req.payload._id)
  // .then(function (user) {
  //   user.getProjects()
  //   .then(function (projects) {
  //     res.json({
  //             success: true,
  //             data: {
  //               projects: projects
  //             },
  //             errors: []
  //     });
  //   });
  // })
  // .catch(function (err) {
  //     res.status(400).json({
  //         success: false,
  //         data: null,
  //         errors: [
  //             'Ошибка при получении списка проектов',
  //             JSON.stringify(err)
  //         ]
  //     });
  // });

  Project.findAll()
  .then(function (projects) {
    res.json({
      success: true,
      data: {
        projects: projects
      },
      errors: []
    });
  });
});

/* ************************************************* Project list *************************************************** */
router.get('/view/:id', function(req, res) {
    Project.findOne({
        where: {
            id: req.params.id
        },
        include: [
            {
                model: User,
                through: {
                    attributes: []
                }
            }
        ]
    }).then(function (project) {
        if (project) {
            res.json({
                success: true,
                data: {
                    project: project
                },
                errors: []
            });
        } else {
            res.status(404).json({
                success: false,
                data: null,
                errors: [
                    'Проект не найден'
                ]
            });
        }
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            data: null,
            errors: [
                'Ошибка при получении проекта',
                JSON.stringify(err)
            ]
        });
    });
});

/* ************************************************ Create project ************************************************** */
router.post('/create', function (req, res) {
  Project.create(req.body)
  .then(function (project) {
    User.findById(req.payload._id).then(function (user) {

      user.addProject(project);
    });

    return project;
  })
  .then(function (project) {
    return res.json({
              success: true,
              data: {
                  project: project
              },
              errors: []
          });
  })
  .catch(function (err) {
      return res.status(400).json({
                success: false,
                data: null,
                errors: [
                    'При создании проекта произошла ошибка',
                    JSON.stringify(err)
                ]
            });
  });
});

/* ************************************************** Set users ***************************************************** */
router.post('/set-users', auth(['manager', 'admin']), function (req, res) {
    var userIds   = req.body.user_ids;
    var projectId = req.body.project_id;

    if (userIds.indexOf(req.user.id) === -1) {
        userIds.push(req.user.id);
    }

    async.waterfall([
        /**
         * @param cb
         */
        function (cb) {
            Project.findOne({
                where: {
                    id: projectId
                }
            }).then(function (project) {
                if (project) {
                    cb(null, project);
                } else {
                    res.status(404).json({
                        success: false,
                        data: null,
                        errors: [
                            'Проект не найден'
                        ]
                    });
                }
            }).catch(function (err) {
                res.status(400).json({
                    success: false,
                    data: null,
                    errors: [
                        'При поиске проекта произошла ошибка',
                        JSON.stringify(err)
                    ]
                });
            });
        },

        /**
         * @param project
         * @param cb
         */
        function (project, cb) {
            User.findAll({
                where: {
                    id: userIds
                }
            }).then(function (users) {
                cb(null, project, users);
            }).catch(function (err) {
                res.status(400).json({
                    success: false,
                    data: null,
                    errors: [
                        'При поиске пользователей произошла ошибка',
                        JSON.stringify(err)
                    ]
                });
            });
        },

        /**
         * @param project
         * @param users
         * @param cb
         */
        function (project, users, cb) {
            project.setUsers(users).then(function () {
                cb(null);
            }).catch(function (err) {
                res.status(400).json({
                    success: false,
                    data: null,
                    errors: [
                        'При добавлении пользователей к проекту произошла ошибка',
                        JSON.stringify(err)
                    ]
                });
            });
        }
    ], function (err) {
        res.json({
            success: true,
            data: {},
            errors: []
        });
    });
});


router.delete('/remove-user', auth(['manager', 'admin']), function (req, res) {
  Project.findById(req.body.project_id)
  .then(function (project) {
    if (!project) {
      return res.status(400).json({
          success: false,
          data: null,
          errors: [
              'При поиске проекта произошла ошибка',
              JSON.stringify(err)
          ]
      });
    }

    project.getUsers({
        where: {
            id: req.payload._id
        }
    })
    .then(function (user) {
      if (!user) {
        return res.status(404).json({
            success: false,
            data: null,
            errors: [
                'Пользователь не найден'
            ]
        });
      }
    });

    return project;
  })
  .then(function (project) {
    project.removeUser(user).then(function () {
      return res.json({
          success: true,
          data: {},
          errors: []
      });
    });
  })
  .catch(function (err) {
      res.status(400).json({
          success: false,
          data: null,
          errors: [
              'При удалении пользователя с проекта произошла ошибка',
              JSON.stringify(err)
          ]
      });
  });
});

module.exports = router;

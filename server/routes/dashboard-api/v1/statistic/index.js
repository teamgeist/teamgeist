var express     = require('express');
var router      = express.Router();
var async       = require('async');
var fs          = require('fs');
var auth        = require(appRoot.concat('/components/auth/auth'));
var entities    = require(appRoot.concat('/components/orm/entities'));
var User        = entities.User;
var Project     = entities.Project;
var Session     = entities.Session;
var Stat        = entities.Stat;

/* ****************************************************************************************************************** */
router.post('/statistics-for-period', function (req, res) {
    var startTime   = req.body.start_time;
    var endTime     = req.body.end_time;
    var projectId   = req.body.project_id;
    var userId      = req.body.user_id;

    Stat.findAll({
        where: {
            save_time: {
                $between: [startTime, endTime]
            },
            status: 'completed'
        },
        include: [
            {
                model: Session,
                required: true,
                attributes: [],
                include: [
                    {
                        model: Project,
                        required: true,
                        attributes: [],
                        where: {
                            id: projectId
                        }
                    },
                    {
                        model: User,
                        required: true,
                        attributes: [],
                        where: {
                            id: userId
                        }
                    }
                ]
            }
        ]
    }).then(function (stats) {
        res.json({
            success: true,
            data: {
                stats: stats
            },
            errors: []
        });
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            data: null,
            errors: [
                'При поиске статистики произошла ошибка',
                JSON.stringify(err)
            ]
        });
    });
});

/* ****************************************************************************************************************** */
router.post('/delete', function (req, res) {

  Stat.findById(req.body.id)
  .then(function (stat) {
    if (!stat) {
      return res.status(404).json({
          success: false,
          data: null,
          errors: [
              'Статистика с таким ID не найдена'
          ]
      });
    }

    if (stat.image) {
        fs.unlink(appRoot.concat('/public/screenshots/').concat(stat.image), function (err) {
            if (err) {
                return res.status(400).json({
                    success: false,
                    data: null,
                    errors: [
                        'Ошибка при удалении файла: '.concat(appRoot.concat('/public/screenshots/').concat(stat.image)),
                        JSON.stringify(err)
                    ]
                });
            }
        });
    }

    stat.destroy().then(function () {
      return res.status(200).json({
          success: true,
          data: {},
          errors: []
      });
    });
  })
  .catch(function (err) {
    return res.status(400).json({
          success: false,
          data: null,
          errors: [
              'При удалении статистики произошла ошибка',
              JSON.stringify(err)
          ]
      });
  });
});

module.exports = router;

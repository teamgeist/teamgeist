var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});

router.use('/auth', require('./auth/index'));
router.use('/user', auth, require('./user/index'));
router.use('/project', auth, require('./project/index'));
router.use('/statistic', auth, require('./statistic/index'));

module.exports = router;

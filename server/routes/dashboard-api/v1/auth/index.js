var express = require('express');
var router  = express.Router();
var passport = require('passport');
// var async   = require('async');
var Promise  = require('bluebird');
var bcrypt  = require('bcrypt');
// var auth    = require(appRoot.concat('/components/auth/auth'));
var helper  = require(appRoot.concat('/components/helper'));
var User    = require(appRoot.concat('/components/orm/entities')).User;

/* ************************************************** Sign In ******************************************************* */
router.post('/sign-in', function(req, res) {
  passport.authenticate('local', function(err, user, info){
    var token;

    // If Passport throws/catches an error
    if (err) {
      res.status(404).json(err);
      return;
    }

    // If a user is found
    if(user){
      token = user.generateJwt();
      res.status(200);
      res.json({
        "token" : token
      });
    } else {
      // If user is not found
      res.status(401).json(info);
    }
  })(req, res);
});

/* ************************************************** Sign Out ****************************************************** */
// router.post('/sign-out', auth(['developer', 'manager', 'admin']), function (req, res) {
//     req.user.auth_token = null;
//     req.user.save().then(function () {
//         res.clearCookie('auth_token').json({
//             success: true,
//             data: {},
//             errors: []
//         });
//     }).catch(function (err) {
//         res.status(400).json({
//             success: true,
//             data: null,
//             errors: [
//                 'При выходе из системы произошла ошибка',
//                 JSON.stringify(err)
//             ]
//         });
//     })
// });

module.exports = router;

var express     = require('express');
var router      = express.Router();
var async       = require('async');
var auth        = require(appRoot.concat('/components/auth/auth'));
var entities    = require(appRoot.concat('/components/orm/entities'));
var User        = entities.User;
var Project     = entities.Project;
var Session     = entities.Session;
var Stat        = entities.Stat;

/* ****************************************************************************************************************** */
/* ************************************************ Get users ******************************************************* */
router.get('/list', function (req, res) {
    User.findAll({
        where: {
            role: {
                $ne: 'blocked'
            }
        }
    }).then(function (users) {
        res.json({
            success: true,
            data: {
                users: users
            },
            errors: []
        });
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            data: null,
            errors: [
                'При поиске пользователей произошла ошибка',
                JSON.stringify(err)
            ]
        });
    });
});

/* ************************************************ Get one user **************************************************** */
router.get('/one-user/:id', function (req, res) {
    var userId = req.params.id;
    User.findById(userId)
    .then(function (user) {
      if (!user) {
        res.status(404).json({
            success: false,
            data: null,
            errors: [
                'User не найден'
            ]
        });
      }
      else {
        res.json({
          success: true,
          data: {
            user: user
          },
          errors: []
        });
      }
    })
    .catch(function (err) {
      res.status(400).json({
          success: false,
          data: null,
          errors: [
              'Ошибка при получении User id',
              JSON.stringify(err)
          ]
      });
    });
});

/* ****************************************************************************************************************** */
/* ************************************************ Get projects **************************************************** */
/* ************************************************ User projects *************************************************** */
router.get('/projects/:id', function (req, res) {
  var userId = req.params.id;
  User.findById(userId)
  .then(function (user) {
    user.getProjects()
    .then(function (projects) {
      if(projects) {
        res.json({
          success: true,
          data: {
            projects: projects
          },
          errors: []
        });
      }
      else {
        res.status(404).json({
           success: false,
           data: null,
           errors: ['Проектов нет']
        });
      }
    });
  })
  .catch(function (err) {
      res.status(400).json({
          success: false,
          data: null,
          errors: [
              'Ошибка при получении списка проектов',
              JSON.stringify(err)
          ]
      });
  });
});

/* *************************************************** Add user to project***************************************************** */
router.post('/add-user', function (req, res) {
    var addUserId   = req.body.user_id;
    var toProjectId = req.body.project_id;
    Project.findById(toProjectId)
    .then(function (project) {
      if (!project) {
        return res.status(404).json({
            success: false,
            data: null,
            errors: [
                'Проект не найден'
            ]
        });
      }

      return User.findById(addUserId)
      .then(function (user) {
        if (!user) {
          return res.status(404).json({
              success: false,
              data: null,
              errors: [
                  'User не найден'
              ]
          });
        }

        project.addUser(user).then(function () {
          return res.json({
              success: true,
              data: {},
              errors: []
          });
        });
      });
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            data: null,
            errors: [
                'При добавлении пользователя к проекту произошла ошибка',
                JSON.stringify(err)
            ]
        });
    });
});

/* ************************************************* Remove user from project**************************************************** */
router.post('/remove-user', function (req, res) {
    var addUserId   = req.body.user_id;
    var toProjectId = req.body.project_id;
    Project.findById(toProjectId)
    .then(function (project) {
      if (!project) {
        return res.status(404).json({
            success: false,
            data: null,
            errors: [
                'Проект не найден'
            ]
        });
      }

      return User.findById(addUserId)
      .then(function (user) {
        if (!user) {
          return res.status(404).json({
              success: false,
              data: null,
              errors: [
                  'User не найден'
              ]
          });
        }

        project.removeUser(user).then(function () {
          return res.json({
              success: true,
              data: {},
              errors: []
          });
        });
      });
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            data: null,
            errors: [
                'При удалении пользователя к проекту произошла ошибка',
                JSON.stringify(err)
            ]
        });
    });
});

/* *************************************************** Create user ***************************************************** */
router.post('/create', function (req, res) {
  User.find({
    where: {
        email: req.body.email
    }
  })
  .then(function (user) {
    if (user) {
      return res.status(400).json({
          success: false,
          data: null,
          errors: [
              'Пользователь с E-mail '.concat(userData.email).concat(' уже существует')
          ]
      });
    }

    User.create(req.body)
    .then(function (user) {
      return res.json({
          success: true,
          data: {
              user: user
          },
          errors: []
      });
    });
  })
  .catch(function (err) {
      return res.status(400).json({
          success: false,
          data: null,
          errors: [
              'Ошибка при создании пользователя',
              JSON.stringify(err)
          ]
      });
  });
});

module.exports = router;

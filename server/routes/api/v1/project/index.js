var express     = require('express');
var router      = express.Router();
var entities    = require(appRoot.concat('/components/orm/entities'));
var User        = entities.User;
var Project     = entities.Project;
var Session     = entities.Session;
var Stat        = entities.Stat;
// var auth        = require(appRoot.concat('/components/auth/auth'));
var orm         = require(appRoot.concat('/components/orm/orm'));
var dbType      = require(appRoot.concat('/config/db')).options.dialect;
var helper      = require(appRoot.concat('/components/helper'));

/* ************************************************* Project list *************************************************** */
router.get('/list', function(req, res) {
    User.findById(req.payload._id).then(function (user) {
      user.getProjects({
          attributes: Object.keys(Project.attributes).concat([
              [
                  orm.literal(
                      helper.sqlConvert('(SELECT `stat`.`image` FROM `stat` ' +
                      'INNER JOIN `tracking_session` ON `tracking_session`.`id` = `stat`.`tracking_session_id` ' +
                      'WHERE `tracking_session`.`user_id` = `user_many_to_many_project`.`user_id` ' +
                      'AND `tracking_session`.`project_id` = `user_many_to_many_project`.`project_id` ' +
                      'AND `stat`.`status` = \'completed\' ' +
                      'ORDER BY `stat`.`id` DESC ' +
                      'LIMIT 1)')
                  ),
                  'last_screen'
              ],
              [
                  orm.literal(
                      helper.sqlConvert('(SELECT SUM(`stat`.`work_time`) FROM `stat` ' +
                      'INNER JOIN `tracking_session` ON `tracking_session`.`id` = `stat`.`tracking_session_id` ' +
                      'WHERE `tracking_session`.`user_id` = `user_many_to_many_project`.`user_id` ' +
                      'AND `tracking_session`.`project_id` = `user_many_to_many_project`.`project_id` ' +
                      'AND `stat`.`status` = \'completed\')')
                  ),
                  'total_work_time'
              ]
          ])
      }).then(function (projects) {
          res.json({
              success: true,
              data: {
                  projects: projects
              },
              errors: []
          });
      }).catch(function (err) {
          res.status(400).json({
              success: false,
              data: null,
              errors: [
                  'Ошибка при получении списка проектов',
                  JSON.stringify(err)
              ]
          });
      });
    });

});

/* ************************************************* View project *************************************************** */
router.get('/view/:id', function(req, res) {
  User.findById(req.payload._id).then(function (user) {
    user.getProjects({
        where: {
            id: req.params.id
        },
        attributes: Object.keys(Project.attributes).concat([
            [
                orm.literal(
                    helper.sqlConvert('(SELECT `stat`.`image` FROM `stat` ' +
                    'INNER JOIN `tracking_session` ON `tracking_session`.`id` = `stat`.`tracking_session_id` ' +
                    'WHERE `tracking_session`.`user_id` = `user_many_to_many_project`.`user_id` ' +
                    'AND `tracking_session`.`project_id` = `user_many_to_many_project`.`project_id` ' +
                    'AND `stat`.`status` = \'completed\' ' +
                    'ORDER BY `stat`.`id` DESC ' +
                    'LIMIT 1)')
                ),
                'last_screen'
            ],
            [
                orm.literal(
                    helper.sqlConvert('(SELECT SUM(`stat`.`work_time`) FROM `stat` ' +
                    'INNER JOIN `tracking_session` ON `tracking_session`.`id` = `stat`.`tracking_session_id` ' +
                    'WHERE `tracking_session`.`user_id` = `user_many_to_many_project`.`user_id` ' +
                    'AND `tracking_session`.`project_id` = `user_many_to_many_project`.`project_id` ' +
                    'AND `stat`.`status` = \'completed\')')
                ),
                'total_work_time'
            ]
        ])
    }).then(function (projects) {
        if (projects.length > 0) {
            res.json({
                success: true,
                data: {
                    project: projects[0]
                },
                errors: []
            });
        } else {
            res.status(404).json({
                success: false,
                data: null,
                errors: [
                    'Проект не найден'
                ]
            });
        }
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            data: null,
            errors: [
                'Ошибка при получении проекта',
                JSON.stringify(err)
            ]
        });
    });
  });
});

module.exports = router;

var express = require('express');
var entities = require(appRoot.concat('/components/orm/entities'));
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});

router.use('/auth', require('./auth/index'));
router.use('/office-loc', auth, require('./office-loc/index'));
router.use('/project', auth, require('./project/index'));
router.use('/tracking', auth, require('./tracking/index'));
router.use('/server-info', auth, require('./server-info/index'));

router.get('/test-login', function (req, res) {
    res.cookie('auth_token', 'qweqweqwe', {
        maxAge: 86400 * 365 * 1000,
        httpOnly: true
    }).json({
        auth_token: 'qweqweqwe'
    });
});

module.exports = router;

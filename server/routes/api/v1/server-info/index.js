var express = require('express');
var router  = express.Router();
var async   = require('async');
var helper  = require(appRoot.concat('/components/helper'));

/* ************************************************ Server Status *************************************************** */
router.all('/status', function (req, res) {
    res.json({
        status: 'OK',
        server_time: helper.time()
    });
});

module.exports = router;

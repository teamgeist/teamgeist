var express     = require('express');
var router      = express.Router();
var fs          = require('fs');
var gm          = require('gm');
var imageMagick = gm.subClass({ imageMagick: true });
var entities    = require(appRoot.concat('/components/orm/entities'));
var orm         = require(appRoot.concat('/components/orm/orm'));
var dbType      = require(appRoot.concat('/config/db')).options.dialect;
var User        = entities.User;
var Project     = entities.Project;
var Session     = entities.Session;
var Stat        = entities.Stat;
var helper      = require(appRoot.concat('/components/helper'));
var auth        = require(appRoot.concat('/components/auth/auth'));
var async       = require('async');
var jsSha256    = require('js-sha256');
var Promise     = require('bluebird');
var securConf   = require(appRoot.concat('/config/security'));
var _           = require('underscore');
var moment      = require('moment');

/* ************************************************ Start tracking ************************************************** */
router.post('/start', function(req, res) {
    var projectId = req.body.project_id;
    var min = helper.currentInterval() * 600;
    var max = min + 599;
    var screenTimeOffset;

    Session.findOne({
      where: {
        project_id: req.body.project_id,
        user_id: req.payload._id,
        stop_time: {
            $between: [min, max]
        }
      }
    })
    .then(function (session) {
      screenTimeOffset = session ? helper.nextScreenTimeOffset() : 0;
      return screenTimeOffset;
    })
    .then(function (screenTimeOffset) {

      return Session.create({
          start_time: helper.time(),
          stop_time: helper.time(),
          status: 'started'
      });
    })
    .then(function (newSession) {

      newSession.setProject(req.body.project_id)
        .then(function (newSession) {
          return newSession.setUser(req.payload._id);
      });

        var token = helper.time().toString().concat(helper.randomString(32));
        var stat = Stat.create({
          creation_time: helper.time(),
          creation_day: moment().format('YYYY-MM-DD'),
          token: token,
          mode: 'online',
          status: 'pending'
        })
        .then(function (stat) {
          stat.setSession(newSession.id);
        });

        return {
          sessionId: newSession.id,
          screenTimeOffset : screenTimeOffset,
          token : token
        };
    })
    .then(function (result) {
      res.json({
          success: true,
          data: {
              start_time: helper.time(),
              session_id: result.sessionId,
              screen_time_offset: result.screenTimeOffset,
              stat_token: result.token
          },
          errors: []
      });
    }).catch(function (err) {
        res.status(400).json({
            success: false,
            data: null,
            errors: [
                'При сохранении сессии произошла ошибка',
                JSON.stringify(err)
            ]
        });
    });
});

/* *********************************************** Stop tracking **************************************************** */
router.post('/stop', function(req, res) {
  Session.findById(req.body.session_id)
  .then(function (session) {
    if (!session) {
      res.status(404).json({
          success: false,
          data: null,
          errors: [
              'Запущенная сессия с таким ID не найдена'
          ]
      });
    }
    session.status = 'stopped';
    session.stop_time = helper.time();
    session.save().then(function () {
      Stat.update(
        {
          token: null,
          status: 'rejected'
        },
        {
            where: {
              status: 'pending',
              tracking_session_id: session.id
            }
        });
    });

    res.json({
        success: true,
        data: {
            stop_time: session.stop_time
        },
        errors: []
    });
  }).catch(function (err) {
      res.status(400).json({
          success: false,
          data: null,
          errors: [
              'При сохранении сессии произошла ошибка',
              JSON.stringify(err)
          ]
      });
  });
});

/* ********************************************** Save activity ***************************************************** */
router.post('/save-activity', function(req, res) {
  var imageBase64 = req.body.screen_image.replace(/^data\:(.*?)base64\,/g, '');

  Stat.findOne({
    where: {
      token: req.body.stat_token,
      status: 'pending'
    },
    include: [
      {
        model: Session,
        required: true,
        where: {
          status: 'started'
        }
      }
    ]
  }).then(function (stat) {
    if (!stat) {
      return res.status(404).json({
        success: false,
        data: null,
        errors: [
          'Токен не найден'
        ]
      });
    }

    var diff = helper.currentInterval() - helper.getIntervalByTime(stat.creation_time);
    Stat.count({
      include: [
          {
              model: Session,
              required: true,
              where: {
                  id: stat.Session.id
              }
          }
      ]
    }).then(function (statCount) {
      if (1 == statCount || 1 == diff) {
        stat.save_time    = helper.time();
        stat.creation_day = moment().format('YYYY-MM-DD');
        stat.work_time    = stat.save_time - stat.creation_time;
        stat.image        = helper.time().toString().concat('_').concat(helper.randomString(32)).concat('.png');
        stat.sources      = JSON.stringify(req.body.activity.sources);
        stat.keyboard     = req.body.activity.keyboard;
        stat.mouse        = req.body.activity.mouse;
        stat.token        = null;
        stat.mode         = 'online';
        stat.status       = 'completed';

        stat.save()
        .then(function (statistic) {
          fs.writeFileSync(appRoot.concat('/public/screenshots/').concat(statistic.image), imageBase64, {encoding: 'base64'});
          return statistic;
        })
        // .then(function (statistic) {
        //   imageMagick(appRoot.concat('/public/screenshots/').concat(statistic.image))
        //   .type('Optimize')
        //   .interlace('Plane')
        //   .samplingFactor(2, 2)
        //   .quality(51)
        //   .write(appRoot.concat('/public/screenshots/').concat(statistic.image), function (err) {
        //     if (err) {
        //       return res.status(400).json({
        //         success: false,
        //         data: null,
        //         errors: ['Image optimization error', JSON.stringify(err)]
        //       });
        //     }
        //   });
        //   return statistic;
        // })
        .then(function (statistic) {
          return Stat.create({
              creation_time: helper.time(),
              creation_day: moment().format('YYYY-MM-DD'),
              token: helper.time().toString().concat(helper.randomString(32)),
              mode: 'online',
              status: 'pending'
          })
          .then(function (newStat) {
              return newStat.setSession(statistic.Session);
          });
        })
        .then(function (newStatistic) {
          return res.json({
              success: true,
              data: {
                  screen_time_offset: helper.nextScreenTimeOffset(),
                  stat_token: newStatistic.token
              },
              errors: []
          });
        });
      } else {
        return res.status(400).json({
            success: false,
            data: null,
            errors: [
              'Невозможно сохранить статистику'
            ]
        });
      }
    });
  })
  .catch(function (err) {
    return res.status(400).json({
        success: false,
        data: null,
        errors: [
          'Ошибка при сохранении статистики',
          JSON.stringify(err)
        ]
    });
  });
});

/* ****************************************************************************************************************** */
router.post('/save-offline-activity', function (req, res) {
  var calcHash        = jsSha256.sha256(JSON.stringify(req.body.data).concat(securConf.offline_activity_salt));
  var projectId       = req.body.project_id;
  var disconnectTime  = req.body.data.disconnection_time;
  var activityList    = req.body.data.list;
  var verifyHash      = req.body.verify_hash;
  /**
   * @param list
   * @param session
   * @param cb
   */
  var recursiveSaving = function (list, session, cb) {
    if (list.length > 0) {
        var activity        = list.shift();
        var fileName        = helper.time().toString().concat('_').concat(helper.randomString(32)).concat('.png');
        var creationTime    = disconnectTime;
        disconnectTime      = activity.screen_time;
        /* ******************************************************************************* */
        Stat.create({
          creation_time: creationTime,
          creation_day: moment(creationTime).format('YYYY-MM-DD'),
          save_time: disconnectTime,
          work_time: disconnectTime - creationTime,
          image: fileName,
          sources: JSON.stringify(activity.activity.sources),
          keyboard: activity.activity.keyboard,
          mouse: activity.activity.mouse,
          token: null,
          mode: 'offline',
          status: 'completed'
        })
        .then(function (stat) {
          session.addStat(stat).then(function () {
            recursiveSaving(list, session, cb);
        });
        // .catch(function (err) {
        //   return res.status(400).json({
        //     success: false,
        //     data: null,
        //     errors: [
        //       'Ошибка при добавлении offline статистики к сессии',
        //       JSON.stringify(err)
        //     ]
        //   });
        // });

        fs.writeFile(
          appRoot.concat('/public/screenshots/').concat(fileName),
          activity.screen_image.replace(/^data\:(.*?)base64\,/g, ''),
          {
            encoding: 'base64'
          },
          function (err) {
            if (err) {
              console.log(err);
            } else {
              // gm(appRoot.concat('/public/screenshots/').concat(fileName))
              // .quality(0)
              // .write(appRoot.concat('/public/screenshots/').concat(fileName), function (err) {
              //   if (err) {
              //     console.log(err);
              //   }
              // });
            }
          }
        );
      });
      // .catch(function (err) {
      //   return res.status(400).json({
      //     success: false,
      //     data: null,
      //     errors: [
      //       'Ошибка при создании offline статистики',
      //       JSON.stringify(err)
      //     ]
      //   });
      // });
    /* ******************************************************************************* */
    } else {
      cb();
    }
  };


  if (calcHash.toLowerCase() === verifyHash.toLowerCase()) {
    Project.findById(req.body.project_id)
    .then(function (project) {
      if (!project) {
        return res.status(404).json({
            success: false,
            data: null,
            errors: [
                'Проект не найден'
            ]
        });
      }

      Session.create({
          description: 'Save offline activity for project ID: '.concat(project.id),
          start_time: disconnectTime,
          stop_time: 0,
          status: 'stopped'
      })
      .then(function (session) {
        project.addSession(session);
        return session;
      })
      .then(function (session) {
        User.findById(req.payload._id)
        .then(function (user) {
          user.addSession(session);
        });
        return session;
      })
      .then(function (session) {
        recursiveSaving(activityList, session, function () {
        });
      })
      .then(function () {
        return res.json({
            success: true,
            data: {},
            errors: []
        });
      });
    });
  }

});

module.exports = router;

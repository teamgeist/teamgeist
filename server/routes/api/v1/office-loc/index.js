var express     = require('express');
var router      = express.Router();
var async       = require('async');
var strtotime   = require('locutus/php/datetime/strtotime');
var entities    = require(appRoot.concat('/components/orm/entities'));
var User        = entities.User;
var Project     = entities.Project;
var Session     = entities.Session;
var Stat        = entities.Stat;
var dbType      = require(appRoot.concat('/config/db')).options.dialect;
var orm         = require(appRoot.concat('/components/orm/orm'));
var helper      = require(appRoot.concat('/components/helper'));

/* ****************************************************************************************************************** */
router.get('/user-stat/:date/:token', function (req, res) {
    var token       = req.params.token;
    var startTime   = strtotime(req.params.date);
    var endTime     = startTime + 86400;

    async.waterfall([
        /**
         * @param cb
         */
        function (cb) {
            if (typeof token === 'string') {
                cb(null);
            } else {
                res.status(400).json({
                    success: false,
                    data: null,
                    errors: [
                        'Некорректный токен'
                    ]
                });
            }
        },

        /**
         * @param cb
         */
        function (cb) {
            User.findOne({
                where: {
                    office_loc_token: token
                }
            }).then(function (user) {
                if (user) {
                    cb(null, user);
                } else {
                    res.status(404).json({
                        success: false,
                        data: null,
                        errors: [
                            'Пользователь не найден'
                        ]
                    });
                }
            }).catch(function (err) {
                res.status(400).json({
                    success: false,
                    data: null,
                    errors: [
                        'При поиске пользователя произошла ошибка',
                        JSON.stringify(err)
                    ]
                });
            });
        },

        /**
         * @param user
         * @param cb
         */
        function (user, cb) {
            user.getProjects({
                attributes: Object.keys(Project.attributes).concat([
                    [
                        orm.literal(
                            helper.sqlConvert('(SELECT SUM(`stat`.`work_time`) FROM `stat` ' +
                            'INNER JOIN `tracking_session` ON `tracking_session`.`id` = `stat`.`tracking_session_id` ' +
                            'WHERE `tracking_session`.`user_id` = `user_many_to_many_project`.`user_id` ' +
                            'AND `tracking_session`.`project_id` = `user_many_to_many_project`.`project_id` ' +
                            'AND `stat`.`status` = \'completed\' ' +
                            'AND `stat`.`creation_time` BETWEEN ' + startTime + ' AND ' + endTime + ')')
                        ),
                        'work_time'
                    ]
                ])
            }).then(function (projects) {
                cb(null, projects);
            }).catch(function (err) {
                res.status(400).json({
                    success: false,
                    data: null,
                    errors: [
                        'Ошибка при получении списка проектов',
                        JSON.stringify(err)
                    ]
                });
            });
        }
    ], function (err, projects) {
        res.json({
            success: true,
            data: {
                work_time: null,
                projects: projects
            },
            errors: []
        });
    });
});

module.exports = router;

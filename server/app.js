var express         = require('express');
var app             = express();
var path            = require('path');
var favicon         = require('serve-favicon');
var logger          = require('morgan');
var cookieParser    = require('cookie-parser');
var bodyParser      = require('body-parser');
var passport        = require('passport');
var cron            = require('node-cron');
var orm             = require('./components/orm/orm');
var jwt             = require('express-jwt');


var auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload'
});


require('./config/passport');

/* ****************************************************************************************************************** */
// App root directory

global.appRoot = __dirname;

/* ****************************************************************************************************************** */
// view engine setup

app.engine('twig', require('atpl').__express);
app.set('devel', false);
app.set('view engine', 'twig');
app.set('view cache', false);
app.set('views', path.join(__dirname, 'views'));

/* ****************************************************************************************************************** */
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

if (app.get('env') === 'development') {
    app.use(logger('dev'));
}

app.use(passport.initialize());

app.use(bodyParser.json({
    limit: '100mb'
}));
app.use(bodyParser.urlencoded({
    extended: false,
    limit: '100mb'
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/* ****************************************************************************************************************** */
// Routes

app.use('/api/v1', require('./routes/api/v1/index'));
app.use('/dashboard-api/v1', require('./routes/dashboard-api/v1/index'));
app.get('*', function (req, res) {
    res.sendFile(__dirname + '/public/index.html');
});


// error handlers
// Catch unauthorised errors
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"message" : err.name + ": " + err.message});
  }
});

/* ****************************************************************************************************************** */
// catch 404 and forward to error handler

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/* ****************************************************************************************************************** */
// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
    app.use(function(err, req, res) {
        res.status(err.status || 500);
        res.render('_system/error', {
            message: err.message,
            error: err
        });
    });
}

/* ****************************************************************************************************************** */
// production error handler
// no stacktraces leaked to user

app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.render('_system/error', {
        message: err.message,
        error: {}
    });
});

/* ****************************************************************************************************************** */

module.exports = app;

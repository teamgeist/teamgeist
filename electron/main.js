'use strict';

const electron = require('electron');
// Module to control application life.
const app  = electron.app;
const Tray =  electron.Tray;
const Menu =  electron.Menu;

// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let appIcon = null;
let keyboardModule;

if (process.platform == "linux") {
    keyboardModule = require(__dirname + '/addon_linux/build/Release/ActivityCapturer.node');
} else if (process.platform == "win32") {
    keyboardModule = require(__dirname + '/addon_win32/build/Release/ActivityCapturer.node');
}

function createWindow () {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        maximizable:false,
        width: 400, 
        height: 600, 
        'webPreferences' : { 
            'webSecurity' : false, 
            'allowDisplayingInsecureContent' : true, 
            'allowRunningInsecureContent' : true 
        }
    });

    // and load the index.html of the app.
    mainWindow.loadURL('file://' + __dirname + '/index.html');
    mainWindow.setResizable(false);

    // Open the DevTools.**************************************************************************************************
    mainWindow.webContents.openDevTools({mode: 'undocked'});
    // ***********************************************************************************************************************

    var electronScreen = electron.screen;
    var size = electronScreen.getPrimaryDisplay().workAreaSize;

    // Emitted when the window is closed.
    mainWindow.on('closed', function() {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        keyboardModule.stop();
        mainWindow = null;
    });

    appIcon = new Tray(__dirname + '/images/favicon.png');
    const contextMenu = Menu.buildFromTemplate([
        {label: 'Item1', type: 'radio'},
        {label: 'Item2', type: 'radio'},
        {label: 'Item3', type: 'radio', checked: true},
        {label: 'Item4', type: 'radio'}
    ]);
    // const menu = new Menu()
    // Menu.setApplicationMenu(menu);

    mainWindow.setMenu(null);

    appIcon.setToolTip('This is my application.');
    appIcon.setContextMenu(contextMenu);
    appIcon.on('click', function(){
        mainWindow.setAlwaysOnTop(true);
        mainWindow.setResizable(false);
        mainWindow.show();
    });
}

// only 1 instance of app can be launched
var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
        if (mainWindow.isMinimized()) {
            mainWindow.restore();
        }
        mainWindow.focus();
    }
});

if (shouldQuit) {
    app.quit();
    return;
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function () {
    createWindow();
    // mainWindow.on('blur', function(){
    //   mainWindow.hide();
    // });
});

// app.on('certificate-error', function(event, webContents, url, error, certificate, callback) {
//   if (url == "https://192.168.137.53:3000/auth/login") {
//     // Verification logic.
//     event.preventDefault();
//     callback(true);
//   } else {
//     callback(false);
//   }
// });

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    keyboardModule.stop();
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
});

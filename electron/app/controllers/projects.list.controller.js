(function (){
  'use strict';

  angular
    .module('app')
    .controller('ProjectsListController', function ($scope, $rootScope, $state, $stateParams, Application, Project, projectList, ActivityTracker, Auth) {

      var vm = this;
      vm.projectList = projectList;
      vm.getProjectList = getProjectList;
      vm.toLogin = toLogin;
      vm.projectList.$promise.then(function (result) {
        return vm.list = result.data.projects;
      });

      function toLogin() {
        ActivityTracker.stop();
        Auth.logout();
      }

      function getProjectList() {
        Project.getProjectList(function (result) {
          result.data.projects.forEach(function(project) {
            project.totalWorkTime = Application.getTotalWorkTime(project.total_work_time);
          });

          return vm.list = result.data.projects;
        });
      }
    })
})();

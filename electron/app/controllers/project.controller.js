(function (){
  'use strict';

  angular
    .module('app')
    .controller('ProjectController', function ($scope, $rootScope, $state, $stateParams, Project, Application, Helper, Activity, ActivityTracker, AppConfig) {

      var async = require('async');
      var data = {};
      var vm = this;

      vm.buttonText = 'play_arrow';
      vm.projectId = $stateParams.id;
      vm.projectName = $stateParams.name;
      vm.totalWorkTime = $stateParams.workedTime || "0h 0m";
      vm.makeScreenshot = Helper.makeScreenshot;
      vm.checkOfflineData = Helper.checkOfflineData;
      vm.saveOfflineData = Helper.saveOfflineData;
      vm.goToSelected = Helper.goToSelected;


      vm.goBack = goBack;
      vm.toggleTracker = toggleTracker;
      vm.sendStatistic = sendStatistic;

      if ($stateParams.lastScreen) {
        vm.screenShot = 'http://' + AppConfig.getConfig().url + '/screenshots/' + $stateParams.lastScreen;
      }

      function stopActivity() {
        if (Helper.getIsActive()) {
          ActivityTracker.stop();
          Activity.stop({ session_id: vm.sessionId });
        }
      }

      function goBack() {
        Helper.goBack().then(function (result) {
          if (result.performExit) {
            stopActivity();
            $state.go('project_list');
          }
        });
      }

      function toggleTracker() {
        if (!Helper.getIsActive()) {
          vm.buttonText = 'pause';

          return Activity.start({ project_id: vm.projectId }, function (response) {
            if (response.success) {
              vm.sessionId = response.data.session_id;
              vm.statToken = response.data.stat_token;
              vm.startOffset = response.data.screen_time_offset * 1000;
              console.log(vm.startOffset/1000/60);
              ActivityTracker.start(vm.sessionId);
              setTimeout(vm.sendStatistic, vm.startOffset);
            }
          });
        }

        vm.buttonText = 'play_arrow';
        return Project.projectInfo({ id: vm.projectId }, function(result) {
          vm.totalWorkTime = Application.getTotalWorkTime(result.data.project.total_work_time);
          stopActivity();
        });
      }


      function sendStatistic() {

        if (!Helper.getIsActive()){
          return false;
        }

        const statistic = ActivityTracker.getStatistic();
        console.log(statistic);
        data = {
          activity: {
            mouse: statistic.mouseCount,
            keyboard: statistic.keyCount
          },
          stat_token: vm.statToken
         };

        async.waterfall([
          makeScreenshotFlow,
          saveActivityFlow,
          getProjectInfoFlow
        ],
        function(err, result) {
        });
      }

      function makeScreenshotFlow(next) {
        vm.makeScreenshot(function (image, displaySources) {
          data.screen_image = vm.screenShot = image;
          data.activity.sources = displaySources;
          next();
        });
      }

      function saveActivityFlow(next) {
        Activity.saveActivity(data, function (res) {
          vm.screenTimeOffset = res.data.screen_time_offset * 1000;
          console.log(vm.sendStatistic);
          console.log(vm.screenTimeOffset/1000/60);
          vm.statToken = res.data.stat_token;
          vm.checkOfflineData(vm.projectId);
          setTimeout(vm.sendStatistic , vm.screenTimeOffset);
        }, function (err) {
          vm.saveOfflineData(data, statistic.screenTime);
          vm.screenOffset =  Math.floor((Math.random() * (100 - 10) + 10) * 10000);
          setTimeout(vm.sendStatistic , vm.screenOffset);
        });
        next();
      }

      function getProjectInfoFlow(next) {
        Project.projectInfo({ id: vm.projectId }, function (result) {
          vm.totalWorkTime = Application.getTotalWorkTime(result.data.project.total_work_time);
          next();
        }, function(err) {
          next();
        });
      }
  });
})();

(function(){
  'use strict';

  angular
    .module('app')
    .controller('MainController', function($scope, $state, Auth, AppConfig, $window, $timeout) {
      var vm = this;
      vm.currentUser = {};
      vm.error_message = '';
      vm.rememberUser = AppConfig.getConfig().rememberUser;
      vm.loginUser = loginUser;
      console.log(process);
      //console.log(process.getuid());
      //console.log(process.hang());
      if (vm.rememberUser) {
          vm.currentUser = Auth.getUser();
      }

      function loginUser() {
        Auth.login(vm.currentUser)
          .then(function() {
            vm.isLoggedIn=true;

            AppConfig.saveOptions({rememberUser: vm.rememberUser});
            if (vm.rememberUser) {
                Auth.saveUser(vm.currentUser);
            } else {
                Auth.removeUser();
            }

            $state.go('project_list');
          },function(err) {
            snackbarContainer.MaterialSnackbar.showSnackbar(err);
            vm.isLoggedIn=false;
          });
        }
    });
})();

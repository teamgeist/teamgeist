(function(){
  'use strict';

  angular
    .module('app')
    .controller('ConfigController', function ($scope, $rootScope, $state, Application, AppConfig, $window, $timeout) {
      var vm = this;
      var config = AppConfig.getConfig();
      vm.serverHost = config.host;
      vm.serverPort = config.port;
      vm.saveConfig = saveConfig;
      function saveConfig() {
        AppConfig.saveOptions({host: vm.serverHost, port: vm.serverPort});
        if(vm.serverPort>0)
        {
          vm.isSaved = true;
          var url = `${vm.serverHost}:${vm.serverPort}`;
          $rootScope.$emit('ConfigController:ConfigChanged');
          $state.go('login');
        }
        else {
          vm.isSaved = false;
        }
      }
    })

})();

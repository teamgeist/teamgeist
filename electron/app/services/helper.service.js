/* Project service */
angular.module('app').factory('Helper', function ($state, $stateParams, Activity, Project, $q) {
  var sha256 = require('js-sha256');
  var storage = require('electron-json-storage');
  var moment = require('moment');
  var electronScreen = require('electron').screen;
  var desktopCapturer = require('electron').desktopCapturer;
  var primaryDisplay = electronScreen.getPrimaryDisplay().size;
  var {dialog} = require('electron').remote;
  var buttons = ['OK', 'Cancel'];
  var displaySources;
  var sessionId;
  var isActive = false;

  return {
    /**
     * Create Notification
     *
     * @param {string} bodyMessage - Notification body message.
     *
     * @return {object} Notification
     */
    createNotification: function (bodyMessage) {
      var notification;

      notification = new Notification('Screen', {
          body: bodyMessage
      });
    },

    /**
     * If timer is active, ask about conformation
     *
     * @return promise
     */
    goBack: function () {
      return $q(function (resolve, reject) {
        if (isActive) {
          dialog.showMessageBox({
            type: 'question',
            buttons: buttons,
            message: 'Timer will be stopped, are u sure?'
          }, function (buttonIndex) {
            const CONFIRM = 0;
            if (buttonIndex === CONFIRM) {
              resolve({performExit: true});
            } else {
              resolve({performExit: false});
            }
          });
        } else {
          resolve({performExit: true});
        }
      });
    },

    /**
      * Create screen shot
      *
      * @param {callback} optional
      * @return {callback} image
      */
    makeScreenshot: function (callback) {
      desktopCapturer.getSources(
        {
          types: ['window', 'screen'],
          thumbnailSize: {
            width: primaryDisplay.width,
            height: primaryDisplay.height
          }
        }, function(error, sources) {
        displaySources = [];
        if (error) throw error;
        image = sources[0].thumbnail.toDataURL();
        for (var i = 0; i < sources.length; i++) {
          displaySources.push(sources[i].name);
        }

        callback(image, displaySources);
      });
    },

    /**
      * Check if offline_data exists,
      * if true, send data and remove it from storage
      *
      * @param {integer} projectId - id of project
      */
    checkOfflineData: function (projectId) {
      storage.get('offline_data', function(error, data) {
        if (error) throw error;
        if (Object.keys(data).length !== 0) {
          var hashedData = sha256(JSON.stringify(data).concat('secret'));
          Project.saveOfflineActivity({data : data, verify_hash: hashedData, project_id: projectId }, function(res) {
            if (res.success) {
              storage.remove('offline_data', function(error) {
                if (error) throw error;
              });
            }
          }, function(err) {
            if (err.status === 400) {
              storage.remove('offline_data', function(error) {
                if (error) throw error;
              });
            }
          });
        }
      });
    },
    
    /**
      * If server is offline, we are saving data to local storage
      *
      * @param {object} userData - data to save.
      * @param {unixTime} time - when app went in offline mode.
      */
    saveOfflineData: function (userData, time) {
      userData.screen_time = time;
      storage.get('offline_data', function(error, data) {
        if (error) throw error;
        if (Object.keys(data).length !== 0) {
          data.list.push(userData);
        } else {
          data.disconnection_time = time;
          data.list = [];
          data.list.push(userData);
        }
        storage.set('offline_data', data, function(error) {
          if (error) throw error;
          console.log(data);
        });

      });
    },

    setIsActive: function (boolean) {
      isActive = boolean;
    },

    getIsActive: function () {
      return isActive;
    },

    setSessionId: function (id) {
      sessionId = id;
    },
  
    getSessionId: function () {
      return sessionId;
    }
  };
});

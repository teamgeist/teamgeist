/* Api service */
angular.module('app').factory('Activity', function ($resource, $rootScope, AppConfig) {
  const self = this;
  let _resource = null;
  
  reinitResource();
  
  $rootScope.$on('ConfigController:ConfigChanged', onConfigChanged);
  
  return self;
  
  // --- Handlers
  function onConfigChanged() {
    reinitResource();
  }
  // --- end Handlers
  // --- Utils
  function reinitResource() {
    const resourceParamDefaults = {
      controller : '@controller',
      route : '@route',
      id : '@id',
    };
    const resourceActions = {
      start: {
        method: 'POST',
        params: {
          controller: 'start',
          route: 'tracking'
        },
        // transformResponse: function (data) {
        //   $rootScope.statToken = angular.fromJson(data).data.stat_token;
        //   $rootScope.sessionId = angular.fromJson(data).data.session_id;
        //   return angular.fromJson(data);
        // }
      },
      stop: {
        method: 'POST',
        params: {
          controller: 'stop',
          route: 'tracking',
          session_id: null
        },
      },
      saveActivity: {
        method: 'POST',
        params: {
          controller: 'save-activity',
          route: 'tracking'
        },
      }
      // transformRequest: function (data) {
      //   data.stat_token = $rootScope.statToken;
      //   return angular.toJson(data);
      // },
      // transformResponse: function (data) {
      //   $rootScope.statToken = angular.fromJson(data).data.stat_token;
      //   return angular.fromJson(data);
      // }
    };
  
    _resource = $resource('http://' + AppConfig.getConfig().url + '/api/v1/:route/:controller/:id', resourceParamDefaults, resourceActions);
    self.start = _resource.start;
    self.stop = _resource.stop;
    self.saveActivity = _resource.saveActivity;
  }
  // --- end Utils
});

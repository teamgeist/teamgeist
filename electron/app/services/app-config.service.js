angular.module('app').factory('AppConfig', AppConfig);

function AppConfig($window) {
  const self = this;
  self.saveOptions = saveOptions;
  self.getConfig = getConfig;

  return self;

  // --- Public
  function saveOptions(options) {
    // url = vm.serverHost + ':' + vm.serverPort;
    // $rootScope.url = url;
    let config = getConfigObject();
    Object.keys(options).forEach((key) => {
      config[key] = options[key];
    });
    if (options.host || options.port) {
      config.host = options.host || config.host || '127.0.0.1';
      config.port = options.port || config.port || '3000';
      config.url = `${config.host}:${config.port}`;
    }
    $window.localStorage.setItem('config', JSON.stringify(config));
  }

  function getConfig() {
    let config = getConfigObject();
    // always needed parameters
    let optionsToSave = {};
    if (!(config.url && config.host && config.port)) {
      let adressOptions = {
        host: config.host || '127.0.0.1',
        port: config.port || '3000'
      };
      adressOptions.url = `${adressOptions.host}:${adressOptions.port}`;
      optionsToSave = Object.assign(config, adressOptions);
    }
    if (!config.rememberUser) {
      optionsToSave.rememberUser = false;
    }
    if (Object.keys(optionsToSave).length > 0) {
      saveOptions(optionsToSave);
      config = Object.assign(config, optionsToSave);
    }
    return config;
  }
  // --- end Public

  // --- Utils
  function getConfigObject() {
    let config = $window.localStorage.getItem('config');
    if (config) {
      config = JSON.parse(config);
    }
    if (!(config  instanceof Object)) {
      config = {};
    }
    return config;
  }
  // --- end Utils
}

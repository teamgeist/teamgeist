/* Authenticate service */

angular.module('app').factory('Auth', function ($location, $rootScope, $http, $window, $q, $state, AppConfig) {
  // var url = $rootScope.url;
  // $rootScope.$watch('url', function (newValue, oldValue) {
  //   url = newValue ? newValue : oldValue;
  // });

  var offlineToken;

  var login = function (user) {
    //  var cb = callback || angular.noop;
    //  var deferred = $q.defer();
    const url = AppConfig.getConfig().url;
    return $http.post('http://' + url + '/api/v1/auth/sign-in', {
      email: user.email,
      passwd: user.password
    }).then(function successCallback(response) {
      console.log('logging in..');
      // deferred.resolve(response);
      // this.setCurrentUser(response.data.user);
      saveToken(response.data.token);
    }, function errorCallback(response) {
      // called asynchronously if an error occurs
      // or server returns response with an error status.
    });
    // .success(function (response) {
    //   console.log('logging in..');
    //   // deferred.resolve(response);
    //   // this.setCurrentUser(response.data.user);
    //   saveToken(response.token);
    //
    //   // currentUser = response.data.user;
    //   // offlineToken = response.data.offline_token;
    //   // return cb(response);
    // });
  };

  var logout = function () {
    console.log('logging out ..');
    currentUser = {};
    projects = [];
    $window.localStorage.removeItem('token');
    $state.go('login');
  };

  var saveToken = function (token) {
    $window.localStorage.token = token;
  };

  var getToken = function () {
    return $window.localStorage.token;
  };

  var saveUser = function({email, password}) {
    $window.localStorage.email = email;
    $window.localStorage.password = password;
  };

  var removeUser = function () {
    $window.localStorage.removeItem('email');
    $window.localStorage.removeItem('password');
  };

  var getUser = function() {
    const email = $window.localStorage.email;
    const password = $window.localStorage.password;

    if (!email || !password) {
      return undefined;
    }

    return {
      email,
      password
    }
  };

  var isLoggedIn = function () {
    var token = getToken();
    var payload;

    if (token) {
      payload = token.split('.')[1];
      payload = $window.atob(payload);
      payload = JSON.parse(payload);

      return payload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  };

  var currentUser = function () {
    if(isLoggedIn()){
      var token = getToken();
      var payload = token.split('.')[1];
      payload = $window.atob(payload);
      payload = JSON.parse(payload);
      return {
        email : payload.email,
        role  : payload.role
        // name : payload.name
      };
    }
  };

  return {
    /**
     * Authenticate user
     *
     * @param  {Object}   user     - login info
     * @param  {Function} callback - optional
     * @return {Promise}
     */
    login: login,

    /**
     * Delete user info
     *
     * @param  {Function}
     */
    logout: logout,

    saveToken: saveToken,

    getToken: getToken,

    isLoggedIn: isLoggedIn,
    /**
     * Gets all available info on authenticated user
     *
     * @return {Object} user
     */
    currentUser: currentUser,

    getUser: getUser,

    saveUser: saveUser,

    removeUser: removeUser,
    /**
      * Set new url from ConfigController
      *
      * @param {string} new url to use
      * @return {string} url
      */
    // setUrl: function(newUrl) {
    //   url = newUrl;
    // },
    //
    // /**
    //   * Get url
    //   *
    //   * @return {string} url
    //   */
    // getUrl: function() {
    //   return url;
    // },

    /**
      * Get offline token for our Api
      * Offline token is required for offline activity route
      *
      * @return {string} offline token
      */
    getOfflineToken: function() {
      return offlineToken;
    }
  };
});

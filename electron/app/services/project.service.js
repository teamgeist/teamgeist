/* Api service */
angular.module('app').factory('Project', function ($resource, $rootScope, Auth, AppConfig, $window) {
  // var url = Auth.getUrl() ? url : $window.localStorage.getItem('url');
  // NOTE: If settings will be able to changing dynamically, url will not change, because instanse of service created once
  // and will be used url, which was in config, when service was initiated
  const self = this;
  let _resource = null;
  
  reinitResource();
  
  $rootScope.$on('ConfigController:ConfigChanged', onConfigChanged);
  
  return self;
  
  // --- Handlers
  function onConfigChanged() {
    reinitResource();
  }
  // --- end Handlers
  
  // --- Utils
  function reinitResource() {
    const resourceParamDefaults = {
      controller : '@controller',
      route : '@route',
      id : '@id'
    };
    const resourceActions = {
      getProjectList: {
        method: 'GET',
        params: {
          controller: 'list',
          route: 'project'
        },
      },
      projectInfo: {
        method: 'GET',
        params: {
          controller: 'view',
          route: 'project',
        },
      },
      saveOfflineActivity: {
        method: 'POST',
        params: {
          controller: 'save-offline-activity',
          route: 'tracking'
        }
      }
    };
    
    _resource = $resource('http://' + AppConfig.getConfig().url + '/api/v1/:route/:controller/:id', resourceParamDefaults, resourceActions);
    self.getProjectList = _resource.getProjectList;
    self.projectInfo = _resource.projectInfo;
    self.saveOfflineActivity = _resource.saveOfflineActivity;
  }
  // --- end Utils
});

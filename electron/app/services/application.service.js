angular.module('app').factory('Application', function($state, Activity, Auth, Helper, ActivityTracker) {
  var moment = require('moment');
  var remote = require('electron').remote;
  var {Menu, app} = require('electron').remote;
  var electron = require('electron');
  var toggleTitle;
  var toggleFunction;

  return {
    getTotalWorkTime: function(seconds) {
      seconds = parseInt(seconds);
      var totalWorkTime;
      if (!seconds) {
        return '0h 0m';
      }
      // string representation of number grouped by 3 digits per group
      // let hoursFormatted = Math.floor(moment.duration(seconds, 'seconds').asHours()).toLocaleString({userGrouping: true});
      let hoursFormatted = Math.floor(moment.duration(seconds, 'seconds').asHours()) + '';
      let minutesFormatted = moment.duration(seconds, 'seconds').minutes() + '';
      return `${hoursFormatted} h ${minutesFormatted} m`;
    }
  };
});

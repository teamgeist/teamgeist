angular.module('app').factory('ActivityTracker', ActivityTracker);

ActivityTracker.$ingect=['Activity', 'Helper'];

function ActivityTracker(Activity, Helper) {
  let vm = this;
  moment = require('moment');
  
  let path;//path to addon
  if(process.platform == "linux"){
    path = './addon_linux/build/Release/ActivityCapturer.node';
  } else if(process.platform == "win32"){
    path = './addon_win32/build/Release/ActivityCapturer.node';
  }
  keyboardTracker = require(path);

  // vm.sessionId = null;

  vm.start = start;
  vm.stop = stop;
  vm.getStatistic = getStatistic;

  return vm;

  // --- Public
  /**
   * Start activity tracker
   *
   * @param sessionId
   */
  function start(sessionId) {
    // vm.sessionId = sessionId;
    keyboardTracker.start();
    Helper.setSessionId(sessionId);
    Helper.setIsActive(true);
  }

  /**
   * Stop activity tracker
   */
  function stop() {
    keyboardTracker.stop();
    Helper.setSessionId(null);
    Helper.setIsActive(false);
  }

  /**
   * Get activity data
   *
   * @returns {{keyCount: *, screenTime, mouseCount: *}}
   */
  function getStatistic() {
    return {
      keyCount: keyboardTracker.getKeyHits(),
      screenTime: moment().format('X'),
      mouseCount: keyboardTracker.getMouseHits()
    }
  }

  // --- end Public
}

//ingection modeules
angular
  .module('app', [
      'ui.router',
      'ngResource',
      'ngAnimate',
      'ngMaterial'
  ]);

//run app
angular
  .module('app')
  .run(runApplication);

//config app
angular
  .module('app')
  .config(configApplication);



runApplication.$inject = ['$stateParams', '$rootScope', '$state'];

function runApplication($stateParams, $rootScope, $state) {
    var moment = require('moment');
    var remote = require('electron').remote;

    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.url = localStorage.getItem('url');

    $rootScope.$on('$viewContentLoaded', function () {
        // $timeout(function () {
        //     //?????????
        // });
    });
}

configApplication.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];

function configApplication($stateProvider, $urlRouterProvider, $httpProvider) {

  $httpProvider.interceptors.push(requestInterceptor);
  $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'app/views/login.html',
            controller: 'MainController',
            controllerAs: 'main',
        })
        .state('project_list', {
            url: '/project_list',
            templateUrl: 'app/views/project_list.html',
            controller: 'ProjectsListController',
            controllerAs: 'projectsList',
            resolve: {
                Project: 'Project',
                Application: 'Application',
                projectList: function($q, Project, Application) {
                    var deferred = $q.defer();
                    Project.getProjectList(function(response) {
                        response.data.projects.forEach(function(project) {
                            project.totalWorkTime = Application.getTotalWorkTime(project.total_work_time);
                        });

                        deferred.resolve(response);
                    });

                    return deferred.promise;
                }
            }
        })
        .state('project', {
            url: '/project/:id',
            params: {
            id: null,
            name: null,
            lastScreen: null,
            workedTime: null
            },
            templateUrl: 'app/views/project.html',
            controller: 'ProjectController',
            controllerAs: 'project'
        })
        .state('config', {
            url: '/config',
            templateUrl: 'app/views/config.html',
            controller : 'ConfigController',
            controllerAs: 'config'
        });

}

function requestInterceptor($q, $window) {
    return {
        request: function(config) {
            config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
            return config;
        },
        requestError: function(config) {
            return config;
        },
        response: function(res) {
            return res;
        },
        responseError: function(res) {
            return $q.reject(res);
        }
    }
}
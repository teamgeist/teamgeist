'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
  return gulp.src('./lib/sass/**/*.sass')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./lib/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./lib/sass/**/*.sass', ['sass']);
});
